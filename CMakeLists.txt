project (udx)

set (UDX_USE_GMP "yes")
set (ROOT_DIR ${CMAKE_SOURCE_DIR})
set (SRC_DIR ${CMAKE_SOURCE_DIR}/src)

# Make third party directory configurable with a default value
if(NOT DEFINED 3RD_PARTY_DIR)
    set(3RD_PARTY_DIR "/Users/rouillie/local" CACHE PATH "Third party libraries directory")
endif()

set (GMP_INC_DIR ${3RD_PARTY_DIR}/include)
set (GMP_LIB_DIR ${3RD_PARTY_DIR}/lib)


include_directories(src ${GMP_INC_DIR})
link_directories (${GMP_LIB_DIR})

set (SOURCES ${SRC_DIR}/udx_def.c
    	     ${SRC_DIR}/udx_error.c
	     	 ${SRC_DIR}/udx_file.c
	     	 ${SRC_DIR}/udx_ieee.c
	     	 ${SRC_DIR}/udx_io.c
	     	 ${SRC_DIR}/udx_mp.c
	     	 ${SRC_DIR}/udx_patent.c
	     	 ${SRC_DIR}/udx_perm.c
	     	 ${SRC_DIR}/udx_sock.c
	     	 ${SRC_DIR}/udx_stream.c
	     	 ${SRC_DIR}/udx_tools.c
	     	 ${SRC_DIR}/udx_udx.c)
if (UDX_USE_GMP)
	set (SOURCES ${SOURCES} ${SRC_DIR}/udx_gmp.c ${SRC_DIR}/udx.c)
	if (UDX_USE_MPFR)
		set (SOURCES ${SOURCES} ${SRC_DIR}/udx_mpfr.c)
		if (UDX_USE_MPFI)
			set (SOURCES ${SOURCES} ${SRC_DIR}/udx_mpfi.c)
		endif()
	endif()
endif()

if(UDX_USE_MAPLE)
	# Find Maple installation
	set(MAPLE_ROOT "/Library/Frameworks/Maple.framework/Versions/2024")

	# Add Maple include directory
	include_directories(${MAPLE_ROOT}/extern/include)

	# Add Maple library directory
	link_directories(${MAPLE_ROOT}/bin.APPLE_ARM64_MACOS)

	set (SOURCES ${SOURCES} ${SRC_DIR}/udx_mpl.c)

endif()

set (INCLUDE_DIRS "${SRC_DIR}/src" )
add_library(${PROJECT_NAME} SHARED ${SOURCES})

if (UDX_USE_GMP)
	target_link_libraries(${PROJECT_NAME} gmp) 
	if (UDX_USE_MPFR)
		target_link_libraries(${PROJECT_NAME} mpfr) 
		if (UDX_USE_MPFI)
			target_link_libraries(${PROJECT_NAME} mpfi) 
		endif()
	endif()
else()
   	target_link_libraries(${PROJECT_NAME}) 
endif()

if(UDX_USE_MAPLE)
	target_link_libraries(${PROJECT_NAME} maplec maple)
endif()

set (TESTS_DIR ${ROOT_DIR}/tests) 

# test_mp.c
add_executable(master_udx ${TESTS_DIR}/test_mp.c)

target_include_directories(master_udx PUBLIC ${INCLUDE_DIRS})
target_link_libraries(master_udx ${PROJECT_NAME} gmp) 
if (UDX_USE_GMP)
	target_compile_options(master_udx PUBLIC -DUDX_USE_GMP=1)
endif()

add_executable(client_udx ${TESTS_DIR}/test_mp.c)
target_include_directories(client_udx PUBLIC ${INCLUDE_DIRS})
target_link_libraries(client_udx ${PROJECT_NAME} gmp) 
if (UDX_USE_GMP)
	target_compile_options(client_udx PUBLIC -DUDX_USE_GMP=1 -DCLIENT_UDX=1)
else()
	target_compile_options(client_udx PUBLIC -DCLIENT_UDX=1)
endif()

# test_mpfr.c
if (UDX_USE_MPFR)
	add_executable(master_mpfr ${TESTS_DIR}/test_mpfr.c)
	target_include_directories(master_mpfr PUBLIC ${INCLUDE_DIRS})
	target_link_libraries(master_mpfr ${PROJECT_NAME} gmp mpfr) 
	target_compile_options(master_mpfr PUBLIC -DUSE_GMP=1 -DUDX_USE_MPFR=1)

	add_executable(client_mpfr ${TESTS_DIR}/test_mpfr.c)
	target_include_directories(client_mpfr PUBLIC ${INCLUDE_DIRS})
	target_link_libraries(client_mpfr ${PROJECT_NAME} gmp mpfr) 
	target_compile_options(client_mpfr PUBLIC -DUSE_GMP=1 -DUDX_USE_MPFR=1 -DCLIENT_UDX=1)
endif()

# test_mpfi.c
if (UDX_USE_MPFI)
	add_executable(master_mpfi ${TESTS_DIR}/test_mpfi.c)
	target_include_directories(master_mpfi PUBLIC ${INCLUDE_DIRS})
	target_link_libraries(master_mpfi ${PROJECT_NAME} gmp mpfr mpfi) 
	target_compile_options(master_mpfi PUBLIC -DUSE_GMP=1 -DUDX_USE_MPFR=1 -DUDX_USE_MPFI=1)

	add_executable(client_mpfi ${TESTS_DIR}/test_mpfi.c)
	target_include_directories(client_mpfi PUBLIC ${INCLUDE_DIRS})
	target_link_libraries(client_mpfi ${PROJECT_NAME} gmp mpfr mpfi) 
	target_compile_options(client_mpfi PUBLIC -DUSE_GMP=1 -DUDX_USE_MPFR=1 -DUDX_USE_MPFI=1 -DCLIENT_UDX=1)
endif()