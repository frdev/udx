module MPFIExt

using UDX

using MPFI

function __init__()
    @info "Initializing MPFIExt"
end

"""
Read an MPFI interval from the UDX channel
"""
function UDX.udx_read_mpfi(ch::Ptr{Cvoid})
    n = BigInterval()
    @ccall $(UDX.lnk_udx_read_mpfi)(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
    return n
end

"""
Write an MPFI interval to the UDX channel
"""
function UDX.udx_write_mpfi(ch::Ptr{Cvoid}, n::BigInterval)
    @ccall $(UDX.lnk_udx_write_mpfi)(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
end

end # module