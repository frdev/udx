module UDX
using Libdl
using Pkg

# *****************************************************
# Load UDX library functions
# *****************************************************

function __init__()
    # Get paths to UDX library
    udx_install_path = @__DIR__
    lib_so_udx = abspath(joinpath(udx_install_path, "../../build/libudx.dylib"))
    libudx = Libdl.dlopen(lib_so_udx)

    # Load all required C functions from UDX library
    global lnk_udx_init_run_tcp_client = Libdl.dlsym(libudx, :udx_init_run_tcp_client)
    global lnk_udx_init_tcp_server_negoc = Libdl.dlsym(libudx, :udx_init_tcp_server_negoc)
    global lnk_udx_init_tcp_client_negoc = Libdl.dlsym(libudx, :udx_init_tcp_client_negoc)
    global lnk_udx_write_i32 = Libdl.dlsym(libudx, :udx_write_i32)
    global lnk_udx_read_i32 = Libdl.dlsym(libudx, :udx_read_i32)
    global lnk_udx_flush = Libdl.dlsym(libudx, :udx_flush)
    global lnk_udx_read_mpz = Libdl.dlsym(libudx, :udx_read_mpz)
    global lnk_udx_write_mpz = Libdl.dlsym(libudx, :udx_write_mpz)
    global lnk_udx_read_mpfr = Libdl.dlsym(libudx, :udx_read_mpfr)
    global lnk_udx_write_mpfr = Libdl.dlsym(libudx, :udx_write_mpfr)
    global lnk_udx_read_mpfi = Libdl.dlsym(libudx, :udx_read_mpfi)
    global lnk_udx_write_mpfi = Libdl.dlsym(libudx, :udx_write_mpfi)

end

# *****************************************************
# Package Availability Checks
# *****************************************************

# """
# Check if MPFI package is available in the Julia registry
# """
# function is_mpfi_available()
#     deps = Pkg.dependencies()
#     return any(dep -> dep.second.name == "MPFI", deps)
# end

# *****************************************************
# Basic UDX Functions
# *****************************************************

"""
Initialize a TCP server that runs a remote process
"""
function udx_init_run_tcp_client(mach::String, path::String, bin_name::String, params::String, to::Int32)
    @ccall $lnk_udx_init_run_tcp_client(mach::Cstring, path::Cstring, bin_name::Cstring, params::Cstring, to::Int32)::Ptr{Cvoid}
end


function udx_init_tcp_server_negoc(mach::String, port::Int32, to::Int32)
    @ccall $lnk_udx_init_tcp_server_negoc(mach::Cstring, port::Int32, to::Int32)::Ptr{Cvoid}
end


function udx_init_tcp_client_negoc(mach::String, port::Int32, to::Int32)
    @ccall $lnk_udx_init_tcp_client_negoc(mach::Cstring, port::Int32, to::Int32)::Ptr{Cvoid}
end

"""
Flush the UDX channel
"""
function udx_flush(ch::Ptr{Cvoid})
    @ccall $lnk_udx_flush(ch::Ptr{Cvoid})::Cint
end

# *****************************************************
# Integer Operations
# *****************************************************

"""
Read a 32-bit integer from the UDX channel
"""
function udx_read_i32(ch::Ptr{Cvoid})
    n = Ref{Int32}(0)  # Create a reference to hold the value
    @ccall $lnk_udx_read_i32(ch::Ptr{Cvoid}, n::Ptr{Cint})::Cint
    return n[]  # Dereference to get the actual value
end

"""
Write a 32-bit integer to the UDX channel
"""
function udx_write_i32(ch::Ptr{Cvoid}, n::Int32)
    @ccall $lnk_udx_write_i32(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cint})::Cint
end

# *****************************************************
# GMP Operations
# *****************************************************

"""
Read a GMP big integer from the UDX channel
"""
function udx_read_mpz(ch::Ptr{Cvoid})
    n = BigInt()
    @ccall $lnk_udx_read_mpz(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
    return n
end

"""
Write a GMP big integer to the UDX channel
"""
function udx_write_mpz(ch::Ptr{Cvoid}, n::BigInt)
    @ccall $lnk_udx_write_mpz(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
end

# *****************************************************
# MPFR Operations
# *****************************************************

"""
Read an MPFR big float from the UDX channel
"""
function udx_read_mpfr(ch::Ptr{Cvoid})
    n = BigFloat()
    @ccall $lnk_udx_read_mpfr(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
    return n
end

"""
Write an MPFR big float to the UDX channel
"""
function udx_write_mpfr(ch::Ptr{Cvoid}, n::BigFloat)
    @ccall $lnk_udx_write_mpfr(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
end

# *****************************************************
# MPFI Operations (Conditional)
# *****************************************************

# Declare the MPFI functions that will be extended
"""
Read an MPFI interval from the UDX channel
"""
function udx_read_mpfi end

"""
Write an MPFI interval to the UDX channel
"""
function udx_write_mpfi end



# Only define MPFI operations if the package is available
# if is_mpfi_available()
#     using MPFI
    
#     export udx_read_mpfi, udx_write_mpfi
    
#     """
#     Read an MPFI interval from the UDX channel
#     """
#     function udx_read_mpfi(ch::Ptr{Cvoid})
#         n = BigInterval()
#         @ccall $lnk_udx_read_mpfi(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
#         return n
#     end

#     """
#     Write an MPFI interval to the UDX channel
#     """
#     function udx_write_mpfi(ch::Ptr{Cvoid}, n::BigInterval)
#         @ccall $lnk_udx_write_mpfi(ch::Ptr{Cvoid}, Ref(n)::Ptr{Cvoid})::Cint
#     end
# end

# *****************************************************
# String Operations
# *****************************************************

"""
Read a string from the UDX channel
"""
function read_string(ch::Ptr{Cvoid})
    # Read string length
    len = udx_read_i32(ch)
    
    # Read string characters
    chars = Vector{UInt8}(undef, len)
    for i in 1:len
        chars[i] = UInt8(udx_read_i32(ch))
    end
    
    return String(chars)
end

"""
Write a string to the UDX channel
"""
function write_string(ch::Ptr{Cvoid}, str::String)
    # Send string length
    len = Int32(length(str))
    udx_write_i32(ch, len)
    
    # Send each character as Int32
    for c in str
        udx_write_i32(ch, Int32(c))
    end
    
    # Flush to ensure all data is sent
    udx_flush(ch)
    
    return len
end

# *****************************************************
# Server Operations
# *****************************************************

"""
Start a UDX server on the specified port. If the port is 0, a port will be chosen automatically.
"""
function start_server(port::Int32=Int32(0))
    channel = udx_init_tcp_client_negoc("localhost", port, Int32(30))
    println("Julia UDX server started on port $port")
    return channel
end

"""
Connect to a UDX server on the specified port
"""
function connect_to_server(port::Int32)
    channel = udx_init_tcp_server_negoc("localhost", port, Int32(30))
    println("Connected to server on port $port")
    return channel
end

# *****************************************************
# Package Redirection
# *****************************************************

"""
Ensure a package is loaded in the Main module
"""
function ensure_package_loaded(package_name::String)
    if !isdefined(Main, Symbol(package_name))
        @eval Main using $(Symbol(package_name))
    end
end

"""
Handle incoming requests by redirecting them to the appropriate package function.
Returns true if the request was handled successfully, false otherwise.
"""
function handle_request(channel::Ptr{Cvoid})
     # Declare variables outside try block so they're available in catch
     package_name = ""
     func_name = ""
     
    try
        # Read package name
        package_name = read_string(channel)
        
        # Read function name
        func_name = read_string(channel)
        
        # Ensure package is loaded
        ensure_package_loaded(package_name)

        @info "Handling request for package $package_name and function $func_name"
        
        # Get the package module and function
        pkg_module = getfield(Main, Symbol(package_name))
        func = getfield(pkg_module, Symbol(func_name))
        # Call the function - it's responsible for reading its own arguments
        # Use Base.invokelatest to handle world age issues
        result = Base.invokelatest(func, channel)
        
        # Send result back (assuming the function handles this)
        udx_flush(channel)
        
        return true
    catch e
        @error "Error handling request" exception=e package_name func_name
        return false
    end
end


"""
Start a server that handles package redirection requests
"""
function start_redirect_server(port::Int32=Int32(0))
    # Initialize UDX server
    channel = connect_to_server(port)
    @info "Julia UDX redirect server started on port $port"
    
    # Main server loop
    while true
        if !handle_request(channel)
            @warn "Failed to handle request, continuing..."
        end
    end
    
    return channel
end

export udx_read_mpfi, udx_write_mpfi

end # module