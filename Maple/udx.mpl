# UDX Interface for Maple
# This module provides communication functions for the UDX protocol

# Load required libraries
UDX:=module()
	description "Maple UDX Interface";

    # Basic UDX Functions
    export udx_init_tcp_server_negoc, udx_init_tcp_client_negoc,
       udx_flush, udx_read_i32, udx_write_i32, udx_read_mpz, udx_write_mpz,
       udx_write_sfloat_to_mpfr, udx_read_string, udx_write_string, 
       udx_write_sfloat, udx_read_sfloat, udx_connect_to_server;
    
    local udx_libpath, mudx_init_tcp_server_negoc, mudx_init_tcp_client_negoc,
       mudx_flush, mudx_read_i32, mudx_write_i32, mudx_read_mpz, mudx_write_mpz,
       mudx_write_sfloat_to_mpfr, mudx_read_mpfr_to_sfloat;
       
option package;
udx_libpath := "/Users/ckatsama/Git/Fabrice/udx_new/udx/build/libudx.dylib";

# Define external C functions from libudx

mudx_init_tcp_server_negoc := define_external(
    'mudx_init_tcp_server_negoc',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_init_tcp_client_negoc := define_external(
    'mudx_init_tcp_client_negoc',
    'MAPLE',
    'LIB'=udx_libpath               
);

mudx_flush := define_external(
    'mudx_flush',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_read_i32 := define_external(
    'mudx_read_i32',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_write_i32 := define_external(
    'mudx_write_i32',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_read_mpz := define_external(
    'mudx_read_mpz',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_write_mpz := define_external(
    'mudx_write_mpz',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_write_sfloat_to_mpfr := define_external(
    'mudx_write_sfloat_to_mpfr',
    'MAPLE',
    'LIB'=udx_libpath
);

mudx_read_mpfr_to_sfloat := define_external(
    'mudx_read_mpfr_to_sfloat',
    'MAPLE',
    'LIB'=udx_libpath
);

# Functions to be exported

udx_init_tcp_server_negoc := mudx_init_tcp_server_negoc;
udx_init_tcp_client_negoc := mudx_init_tcp_client_negoc;
udx_flush := mudx_flush;
udx_read_i32 := mudx_read_i32;
udx_write_i32 := mudx_write_i32;
udx_read_mpz := mudx_read_mpz;
udx_write_mpz := mudx_write_mpz;
udx_write_sfloat := mudx_write_sfloat_to_mpfr;

udx_read_sfloat := proc(ch)
    local sfloat;
    sfloat := [mudx_read_mpfr_to_sfloat(ch)];
    return sfloat[1]*2^(sfloat[2]);
end proc;


udx_read_string := proc(ch)
    local len, chars, i;
    
    # Read string length
    len := mudx_read_i32(ch);
    
    # Read string characters
    chars := "";
    for i from 1 to len do
        chars := cat(chars, StringTools:-Char(mudx_read_i32(ch)));
    end do;
    
    return chars;
end proc;

udx_write_string := proc(ch, str::string)
    local len, i;
    
    # Send string length
    len := StringTools:-Length(str);
    mudx_write_i32(ch, len);
    
    # Send each character as Int32
    for i from 1 to len do
        mudx_write_i32(ch, StringTools:-Ord(str[i]));
    end do;
    
    # Flush to ensure all data is sent
    mudx_flush(ch);
    
    return len;
end proc;

# Server Operations

udx_connect_to_server := proc(port::integer)
    local channel;
    channel := mudx_init_tcp_server_negoc("localhost", port, 30);
    printf("Connected to server on port %d\n", port);
    return channel;
end proc;

end module:

