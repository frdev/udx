## Building UDX Library

### Compilation Steps
1. Create a build directory:
   ```bash
   mkdir build 
   ```

2. In the files tests/test_mp.c, tests/test_mpfr.c and tests/test_mpfi.c, you may need to change the path of the udx build directory in the following line:
   ```c
   _udx_init_tcp_client_negoc_0(channel,"localhost",
			              "path/to/udx/build/directory/",
			              "client_mpfi","",to_1); 
   ```
   to the path of the udx build directory.

3. Configure the project:

   Optional flags for third-party libraries:
   - If using MPFR, GMP must also be used
   - If using MPFI, MPFR must also be used

   Add the respective flags 

   -DUDX_USE_GMP=1
   -DUDX_USE_MPFR=1
   -DUDX_USE_MPFI=1

   in the command below:

   ```bash
   cmake -D3RD_PARTY_DIR=/path/to/third-party -B build -DUDX_USE_GMP=1 -DUDX_USE_MPFR=1 -DUDX_USE_MPFI=1
   ```

   Optional flags for Maple:
   ```bash
   cmake -D3RD_PARTY_DIR=/path/to/third-party -B build -DUDX_USE_GMP=1 -DUDX_USE_MPFR=1 -DUDX_USE_MPFI=1 -DUDX_USE_MAPLE=1
   ```

3. Build the project:
   ```bash
   cd build && make
   ```
### Running Tests
To run a test:

1. Make sure you've compiled the project first
2. From the build directory, execute:
   ```bash
   ./master_udx
   ```
    or if you use also mpfr:
   ```bash
   ./master_mpfr
   ```
    or if you use also mpfi:
   ```bash
   ./master_mpfi
   ```

## Julia UDX
To use the UDX.jl package:

1. Navigate to the Julia/ directory of this repository:
```bash
cd Julia/
```

2. Start Julia REPL:
```bash
julia
```

3. Enter package mode by pressing `]`, then add the package locally:
```julia
pkg> dev .
```

### Interprocess Communication Example
To demonstrate communication between two Julia processes:

1. Open two terminal windows. In the first terminal (Master):
```julia
using UDX

# Start the server
ch = UDX.start_server()
# Note the port number from the output

# Send a big integer
UDX.udx_write_mpz(ch, BigInt(123))
UDX.udx_flush(ch)

# Load MPFI for interval arithmetic
using MPFI

# Read the interval that will be sent from the client
a = UDX.udx_read_mpfi(ch)
println("Received interval: ", a)
println("Precision: ", precision(a))
```

2. In the second terminal (Client):
```julia
using UDX

# Connect to the server (replace PORT with the number from the server output)
ch = UDX.connect_to_server(Int32(PORT))

# Read the big integer sent by the server
received_num = UDX.udx_read_mpz(ch)
println("Received number: ", received_num)

# Load MPFI and send an interval back
using MPFI
UDX.udx_write_mpfi(ch, BigInterval(1//3; precision=14))
UDX.udx_flush(ch)
```



