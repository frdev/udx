#include "udx.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "udx_stream.h"
#include "udx_tools.h"
#include "udx_gmp.h"

void * udx_init_run_tcp_client(char * mach,char * dir,char * bin_name,char * param,int32_t to){
    _udx_stream_rep * channel=malloc(sizeof(_udx_stream_rep));
    struct timeval to_1;
    to_1.tv_sec=to;
    to_1.tv_usec=0;
    fprintf(stderr,"\n %s %s %s %s\n",mach,dir,bin_name,param);
    _udx_init_tcp_client_negoc_0(channel,mach,dir,bin_name,param,to_1);
    return(channel);
}

void * udx_init_tcp_server_negoc(char * mach,int32_t port,int32_t to){
  _udx_stream_rep * channel=malloc(sizeof(_udx_stream_rep));
  struct timeval to_1;
  to_1.tv_sec=to;
  to_1.tv_usec=0;
  _udx_init_tcp_server_negoc(channel,mach,port,to_1);
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  return(channel);
}

void * udx_init_tcp_client_negoc(char * mach,int32_t port,int32_t to){
  _udx_stream_rep * channel=malloc(sizeof(_udx_stream_rep));
  struct timeval to_1;
  to_1.tv_sec=to;
  to_1.tv_usec=0;
  _udx_init_tcp_client_negoc_1(channel,mach,port,to_1);
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  return(channel);
}

int udx_read_i_n(void * ch, void * tab, const uint32_t unit_size, const uint32_t nb_element){
  return(read_n_int(&(((_udx_stream_rep *)ch)->i),(UDX_IT *) (tab),unit_size,unit_size*nb_element));
}
int udx_write_i_n(void * ch, void * tab, const uint32_t unit_size, const uint32_t nb_element){
  return(send_n_int(&(((_udx_stream_rep *)ch)->i),(UDX_IT *) (tab),unit_size,unit_size*nb_element));
}
int udx_read_mpz(void * ch,void * src){
  return(_udx_read_mpz((_udx_stream_rep *)ch,(mpz_ptr) src));
}
int udx_write_mpz(void * ch,void * dst){
  return(_udx_send_mpz((_udx_stream_rep *)ch,(mpz_ptr) dst));
}

int    udx_read_i32(void * ch,int32_t * n){
  return(read_i32(((_udx_stream_rep *)ch),n));
}

int    udx_write_i32(void * ch,int32_t * n){
  return(send_i32(((_udx_stream_rep *)ch),n));
}
 
int    udx_flush(void * ch){
  return(_udx_sflush(((_udx_stream_rep *)ch)));
}
