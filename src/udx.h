#ifndef _udx_h
#define _udx_h
#include <stdint.h>

extern void * udx_init_run_tcp_client(char * mach,char * dir,char * bin_name,char * param,int32_t to);
extern void * udx_init_tcp_server_negoc(char * mach,int32_t port,int32_t to);
extern void * udx_init_tcp_client_negoc(char * mach,int32_t port,int32_t to);
extern int    udx_read_i32(void * ch,int32_t * n);
extern int    udx_write_i32(void * ch,int32_t * n);
extern int    udx_flush(void * ch);
extern int    udx_read_mpz(void * ch,void * dst);
extern int    udx_write_mpz(void * ch,void * src);

#endif
