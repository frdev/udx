#include "udx_def.h"

void * _udx_alloc_default(unsigned int i)
{
  return((void *)(malloc(i)));
}
void * _udx_realloc_default(void *p,unsigned int i)
{
  return((void *)(realloc(p,i)));
}

void _udx_free_default(void *p)
{
  free(p);
}
void * (* _udx_alloc)(unsigned int)=_udx_alloc_default;
void * (* _udx_realloc)(void *,unsigned int)=_udx_realloc_default;
void  (* _udx_free)(void *)=_udx_free_default;

int _udx_read_timeout=-1;
int _udx_write_timeout=-1;

void set_read_timeout(const int to)
{
  _udx_read_timeout=to;
}
void set_write_timeout(const int to)
{
  _udx_write_timeout=to;
}
int get_read_timeout()
{
  return(_udx_read_timeout);
}
int get_write_timeout()
{
  return(_udx_write_timeout);
}
