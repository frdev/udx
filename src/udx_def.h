#ifndef __udx_def_h
#define __udx_def_h
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

extern void * (* _udx_alloc)(unsigned int);
extern void * (* _udx_realloc)(void *ptr,unsigned int);
extern void  (* _udx_free)(void *);

typedef int16_t        UDX_I16;
typedef uint16_t       UDX_UI16;
typedef int32_t        UDX_I32;
typedef uint32_t       UDX_UI32;
typedef int64_t        UDX_I64;
typedef uint64_t       UDX_UI64;
typedef char *         UDX_STRING;
typedef unsigned char  UDX_IT;

typedef int            UDX_FD;

#define UDX_STATIC_INLINE static
#define UDX_STATIC static


#define UDX_PTR_NULL 0

#define UDX_ALLOC_ATOMIC(a) _udx_alloc(a)
#define UDX_SIZEOF(a) sizeof(a)
#define UDX_MEMCPY(a,b,c) memcpy(a,b,c)
#define UDX_MEMCMP(a,b,c) memcmp(a,b,c)
#define UDX_MEMSET(a,b,c) memset(a,b,c)
#define UDX_BZERO(a,b) bzero(a,b)
#define UDX_MIN(a,b) ((a<b) ?a :b)

extern int _udx_read_timeout;
extern int _udx_write_timeout;
extern void set_read_timeout(const int);
extern void set_write_timeout(const int);
extern int get_read_timeout();
extern int get_write_timeout();

#define UDX_BIG_SIZE 255

#endif /* __udx_def_h */








