#include "udx_error.h"


const char *_udx_errlist[]={\
					  "No error",\
					  "System error",\
					  "Connection time out",\
					  "Data too big",\
					  "Network error",\
					  "Not our correspondant",\
					  "buffer overflow",\
					  "Read Timeout",\
			                  "Write Timeout",\
			                  "WRITE",\
			                  "READ",\
                                          "SELECT",\
                                          "UNKNOWN ERROR",\
                                          "WinSock Error",\
                                          "Size (Read)",\
                                          "Size (Write)",\
			   "Select Socket",\
			   "Accept Connection",\
			   "Bind Socket",\
			   "Sockname",\
			   "Listen Socket",\
			   "Unknown host",\
			   "Connect",\
			   "Error trunc (long)",\
			   "Socket "\
};


int _udx_error=0;

void _udx_seterror(const int e)
{
  if (_udx_noerror()) {
    _udx_error=e;
  }
}

const char * _udx_geterrormsg(const int e)
{
  return(_udx_errlist[e]);
}

int _udx_geterror()
{
  return(_udx_error);
}

void _udx_reseterror()
{
  _udx_error=0;
}

int _udx_noerror()
{
  return(_udx_error==0);
}

