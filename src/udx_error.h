#ifndef __udx_error_h
#define __udx_error_h

#include <errno.h>

#define UDX_NO_ERROR 0
#define UDX_SYSTEM_ERROR 1
#define UDX_TIMEOUT_CONNECT 2
#define UDX_DATA_TOO_BIG 3
#define UDX_NETWORK_ERROR 4
#define UDX_NOT_OUR_CORRESP 5
#define UDX_BUFFER_OVERFLOW 6
#define UDX_TIMEOUT_READ 7
#define UDX_TIMEOUT_WRITE 8
#define UDX_WRITE 9
#define UDX_READ 10
#define UDX_SELECT 11
#define UDX_UNKNOWN_ERROR 12
#define UDX_WINSOCK_ERROR 13
#define UDX_SIZE_READ 14
#define UDX_SIZE_WRITE 15
#define UDX_SELECT_SOCKET 16
#define UDX_ACCEPT 17
#define UDX_BIND 18
#define UDX_SOCKNAME 19
#define UDX_LISTEN 20
#define UDX_UNKNOWN_HOST 21
#define UDX_CONNECT 22
#define UDX_TR_UNK 23
#define UDX_SOCKET 24

/**** signature des fonctions ****/

extern void         _udx_seterror(const int);
extern const char * _udx_geterrormsg(const int);
extern int          _udx_geterror();
extern void         _udx_reseterror();
extern int          _udx_noerror();

extern int _udx_error;

#endif /* _udx_error_h */

