#include "udx_file.h"

int open_file(const char * fname, const int flags)
{
  int a;
#ifndef CYGWIN
#ifdef WIN32
  _fmode=_O_BINARY;
  if ((a=_open(fname,flags,_S_IREAD | _S_IWRITE))<0) 
    _udx_error=UDX_SYSTEM_ERROR;
#else
  if ((a=open(fname,flags,S_IRUSR|S_IWUSR))<0) _udx_error=UDX_SYSTEM_ERROR;
#endif
  return(a);
#else 
  return(0);
#endif
}

static int something_to_read_file(int fd)
{
#ifndef CYGWIN
#ifdef WIN32
  if(eof(fd)) {
    _udx_seterror(UDX_READ);
    return( 0 ); // (andi)
  }
#endif
#endif
  return( 1 );
}



int real_read_file(char * out1,const int ofset,const int to_send,const int fd) {
  int res = 0;
  while ((_udx_noerror()) && ((res=something_to_read_file(fd))<1)) {};
  if(_udx_error) {
    return(0);
  }
  errno=0;
  res=read(fd,(char *) (out1 + ofset),to_send);
  if ((res==0)|| (errno)) _udx_seterror(UDX_READ);
  if (res<0){
    _udx_seterror(UDX_READ);
    return(0);
  }
  return(res);
}

static int can_write_file(int fd)
{
#ifndef WIN32
	int res=0;
  fd_set writefds;
  FD_ZERO(&writefds);
  FD_SET((unsigned int)fd,&writefds);
  if ((res=select(fd+1,0,&writefds,0,NULL)) == -1)
    {
      _udx_seterror(UDX_SELECT);
      return -1;
    }
  if (FD_ISSET(fd,&writefds)) {
    return(1);
  }  
  return(0);
#else 
  return(1);
#endif
}

int real_write_file(char * out1,const int ofset,const int to_send,const int fd) {
  int res;
  while ((_udx_noerror()) && (can_write_file(fd)< 1)) {};
  if(_udx_error) return(0);
  res=write(fd,(void *) (out1 + ofset),to_send);
  if(res<0) {
    _udx_seterror(UDX_WRITE);
    return(0);
  }
  else return(res);
}
