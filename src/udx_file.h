#ifndef __udx_file_h
#define __udx_file_h

#include "udx_error.h"

#ifdef WIN32
#include <io.h>
#include <stdlib.h>
#else
#include <unistd.h>
#include <sys/time.h>
#endif
#include "stdio.h"
#include "string.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
extern int open_file(const char *, const int flags);
extern FILE * open_stream(const char *,const char *);
extern int real_read_file(char *,const int,const int,const int);
extern int real_write_file(char *,const int,const int,const int);
#endif
