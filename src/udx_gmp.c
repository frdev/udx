#include "udx_gmp.h"

int _udx_send_mpz(_udx_stream_rep *c,mpz_srcptr tmp1)
{
  int i=0;
  int tmp=tmp1->_mp_size;
  send_i32(c,(UDX_IT *)(&tmp));
  if(tmp<0) tmp=-tmp;
  send_n_int(&((c)->i),(UDX_IT *) (tmp1->_mp_d),8,8*(tmp));
  return(1);
}

/* mpz 64 little endian */
int _udx_read_mpz(_udx_stream_rep *c,mpz_ptr tmp1)
{
  int tmp=0;
  int i=0;
  //mpz_clear(tmp1);
  read_i32(c,(UDX_IT *)(&tmp));
  if (tmp<0) tmp1->_mp_alloc=-(tmp);
  else tmp1->_mp_alloc=tmp;
  mpz_init2(tmp1,(tmp1->_mp_alloc)*(sizeof(mp_limb_t))*8);
  read_n_int(&((c)->i),(UDX_IT *) (tmp1->_mp_d),8,8*(tmp1->_mp_alloc));
  if(tmp1->_mp_alloc>0){
    if(tmp<0) tmp1->_mp_size=-(tmp1->_mp_alloc);
    else tmp1->_mp_size=tmp1->_mp_alloc;
  }
  else {
    tmp1->_mp_size=0;
  }
  return(1);
}
