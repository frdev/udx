
#ifndef __udx_gmp_h
#define __udx_gmp_h
#include "udx_stream.h"
#include "gmp.h"

extern int _udx_send_mpz(_udx_stream_rep *c,mpz_srcptr tmp1);
extern int _udx_read_mpz(_udx_stream_rep *c,mpz_ptr tmp1);

#endif