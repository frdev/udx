#include "udx_ieee.h"

/* UDX_STATIC int __udx_ieee_initialized=0;*/


UDX_STATIC void initialise_tableau_ieee (UDX_IT **tab) {
  UDX_UI32 i;
  for (i=0;i<UDX_MAX_SIZE_IEEE;i++) 
    tab[i]=UDX_PTR_NULL;
}


UDX_STATIC void internal_perm_ieee(int s,UDX_IT * target,const UDX_IT *basic,const int su)
     /* ieee vu comme une succession de int dans le sens
	naturel */
{
  int i,j;
  if(basic) {
    UDX_MEMCPY(&((target)[0]),
		basic,su);
  }
  else {
    for(i=0;i<su;i++) target[i]=i;
  }
  for(i=su;i<s;i+=su) {
    UDX_MEMCPY(&(target[i]),&(target[0]),su);
    for(j=0;j<su;j++) {
      (target)[i+j]+=i;
    }
  }
}

UDX_STATIC void rev_internal_perm_ieee(const int s,UDX_IT *target,
				   const UDX_IT *basic,const int su)
     /* ieee vu comme une succession de int dans le sens
	inverse */
{
  int i,k,j;
  if (basic) {
    UDX_MEMCPY(&(target[s-su]),
		basic,su);
  }
  else {
    for(j=0,i=s-su;i<s;i++,j++) target[i]=j;
  }
  for(k=su,i=(s-2*su);i>-1;i-=su,k+=su) {
    UDX_MEMCPY(&(target[i]),&(target[s-su]),su);
    for(j=0;j<((int) sizeof(UDX_UI32));j++) {
      (target)[i+j]+=k;
    }
  }
}

UDX_STATIC void cons_perm_ieee(_udx_io_rep *can,_udx_ieee_rep *dst)
{
  int su=sizeof(UDX_UI32);
  int sf=sizeof(float);
  int sd=sizeof(double); 
  /*  if (__udx_ieee_initialized==0) {*/
    initialise_tableau_ieee(dst->tab_externe_to_interne_ieee);
    if (dst->sens_ieee_target != dst->sens_ieee_local) {
      /* cas sens ieee differents */
      dst->tab_externe_to_interne_ieee[sf]=(UDX_IT *)UDX_ALLOC_ATOMIC(sf);  
      dst->tab_externe_to_interne_ieee[sd]=(UDX_IT *)UDX_ALLOC_ATOMIC(sd);  
      rev_internal_perm_ieee(sizeof(float),
			     dst->tab_externe_to_interne_ieee[sf],
			     can->tab_externe_to_interne[su],su);
      rev_internal_perm_ieee(sizeof(double),
			     dst->tab_externe_to_interne_ieee[sd],
			     can->tab_externe_to_interne[su],su);
    }
    else {
      /* cas sens ieee pareils */
      if(can->tab_externe_to_interne[su]) {
	/* si necessite permutations 32 bits */
	dst->tab_externe_to_interne_ieee[sf]=(UDX_IT *)UDX_ALLOC_ATOMIC(sf);  
	dst->tab_externe_to_interne_ieee[sd]=(UDX_IT *)UDX_ALLOC_ATOMIC(sd);  
	internal_perm_ieee(sizeof(float),
			   dst->tab_externe_to_interne_ieee[sf],
			   can->tab_externe_to_interne[su],su);
	internal_perm_ieee(sizeof(double),
			   dst->tab_externe_to_interne_ieee[sd],
			   can->tab_externe_to_interne[su],su);      
      }
      else {
	/* tout pareil ! */
	dst->tab_externe_to_interne_ieee[sf]=UDX_PTR_NULL;
	dst->tab_externe_to_interne_ieee[sd]=UDX_PTR_NULL;
      }
    }
    /*    __udx_ieee_initialized=1;
	  }*/
}

int read_init_infos_ieee_negoc(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  int res;
  _udx_read_init(can);
  res=read_n_int(can,(UDX_IT *) (&(dst->sens_ieee_target)),4,4); 
  cons_perm_ieee(can,dst);
  return(res);
}

int send_init_infos_ieee_negoc(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  int res;
  _udx_send_init(can);
  res=send_n_int(can,(UDX_IT *) &(dst->sens_ieee_local),4,4); 
  _udx_flush(can);
  initialise_tableau_ieee(dst->tab_interne_to_externe_ieee);
  dst->tab_interne_to_externe_ieee[sizeof(float)]=UDX_PTR_NULL;
  dst->tab_interne_to_externe_ieee[sizeof(double)]=UDX_PTR_NULL;
  return(res);
}

int read_init_infos_ieee_ieee(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  _udx_read_init(can);
  dst->sens_ieee_target=1;
  cons_perm_ieee(can,dst);
  initialise_tableau_ieee(dst->tab_interne_to_externe_ieee);
  dst->tab_interne_to_externe_ieee[sizeof(float)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(float)],sizeof(float));
  dst->tab_interne_to_externe_ieee[sizeof(double)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(double)],sizeof(double));
  return(1);
}

int send_init_infos_ieee_ieee(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  _udx_send_init(can);
  dst->sens_ieee_target=1;
  cons_perm_ieee(can,dst);
  initialise_tableau_ieee(dst->tab_interne_to_externe_ieee);
  dst->tab_interne_to_externe_ieee[sizeof(float)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(float)],sizeof(float));
  dst->tab_interne_to_externe_ieee[sizeof(double)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(double)],sizeof(double));
  return(1);
}

int read_init_infos_ieee_rev(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  _udx_read_init(can);
  dst->sens_ieee_target=0;
  cons_perm_ieee(can,dst);
  initialise_tableau_ieee(dst->tab_interne_to_externe_ieee);
  dst->tab_interne_to_externe_ieee[sizeof(float)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(float)],sizeof(float));
  dst->tab_interne_to_externe_ieee[sizeof(double)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(double)],sizeof(double));
  return(1);
}

int send_init_infos_ieee_rev(_udx_io_rep * can,_udx_ieee_rep *dst)
{
  _udx_send_init(can);
  dst->sens_ieee_target=0;
  cons_perm_ieee(can,dst);
  initialise_tableau_ieee(dst->tab_interne_to_externe_ieee);
  dst->tab_interne_to_externe_ieee[sizeof(float)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(float)],sizeof(float));
  dst->tab_interne_to_externe_ieee[sizeof(double)]=inv_permut(dst->tab_externe_to_interne_ieee[sizeof(double)],sizeof(double));
  return(1);
}

#include <stdio.h>

int send_n_ieee(_udx_io_rep * u,_udx_ieee_rep *a,const UDX_IT* b,const UDX_UI32 c,const UDX_UI32 d)
{
  /*  fprintf(stderr,"\n send ieee\n");*/
  if(!(a->sent_perm)) {
    a->sent_perm=1;
    a->send_init_infos_ieee(u,a);
  }
  return(((d>c) ? send_n_p(u,b,c,d,a->tab_interne_to_externe_ieee[c]): send_1_p(u,b,c,a->tab_interne_to_externe_ieee[c])));
}

int read_n_ieee(_udx_io_rep * u,_udx_ieee_rep *a,UDX_IT* b,const UDX_UI32 c,const UDX_UI32 d)
{
  /*  fprintf(stderr,"\n read ieee\n");*/
  if(!(a->received_perm)) {
    a->received_perm=1;
    a->read_init_infos_ieee(u,a);
  }
  return(((d>c) ? read_n_p(u,b,c,d,a->tab_externe_to_interne_ieee[c]): read_1_p(u,b,c,a->tab_externe_to_interne_ieee[c])));
}
int init__udx_ieee(_udx_io_rep * can,_udx_ieee_rep *dst,
		 int (*read_i)(_udx_io_rep *,_udx_ieee_rep *),
		 int (*send_i)(_udx_io_rep *,_udx_ieee_rep *))
{
  dst->sens_ieee_local=detect_ieee();
  dst->received_perm=0;
  dst->sent_perm=0;
  dst->send_init_infos_ieee=send_i;
  dst->read_init_infos_ieee=read_i;
  return(1);
}
