#ifndef __udx_ieee_h
#define __udx_ieee_h

#include "udx_error.h"
#include "udx_perm.h"
#include "udx_io.h"
#if defined(WIN32) && defined(__cplusplus)
}
#endif
#include <math.h>
#if defined(WIN32) && defined(__cplusplus)
#include <math.h>
extern "C" {
#endif

#define UDX_MAX_SIZE_IEEE 12

typedef struct _udx_ieee_rep{
  UDX_IT *tab_externe_to_interne_ieee[UDX_MAX_SIZE_IEEE]; 
  UDX_IT *tab_interne_to_externe_ieee[UDX_MAX_SIZE_IEEE]; 
  UDX_UI32 sens_ieee_local;
  UDX_UI32 sens_ieee_target;
  UDX_UI32 received_perm;
  UDX_UI32 sent_perm;
  int (*send_init_infos_ieee)  (_udx_io_rep *,struct _udx_ieee_rep *);
  int (*read_init_infos_ieee)  (_udx_io_rep *,struct _udx_ieee_rep *);
}_udx_ieee_rep;

extern int (*detect_ieee)(void);

extern int init__udx_ieee(_udx_io_rep *,_udx_ieee_rep *,
			int (*read_init)(_udx_io_rep *,_udx_ieee_rep *),
			int (*send_init)(_udx_io_rep *,_udx_ieee_rep *));

extern int send_n_ieee(_udx_io_rep *,_udx_ieee_rep *,const UDX_IT*,
		   const UDX_UI32,const UDX_UI32);
extern int read_n_ieee(_udx_io_rep *,_udx_ieee_rep *,UDX_IT*,
		   const UDX_UI32,const UDX_UI32);

extern int send_init_infos_ieee_negoc(_udx_io_rep *,_udx_ieee_rep *);
extern int read_init_infos_ieee_negoc(_udx_io_rep *,_udx_ieee_rep *);


#endif
