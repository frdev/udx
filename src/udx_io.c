/***************


	  UDX   Universal Data eXchange


**************/


/****************************************************************
 Definition des permutations,
****************************************************************/

#include "udx_io.h"

/* UDX_STATIC UDX_UI32 prot_uninitialized=1;*/
unsigned int UDX_DEFAULT_BUF_SIZE=2048;

/* ------------------------------------- */
/*    permutation tableau optimisee      */
/* ------------------------------------- */

#define permut_opt(d,s,p,n) \
{ \
  UDX_UI32 c; \
  for (c=0;c<n;c++) (d)[c]=(s)[(p)[c]];  \
}

#define permut_tab_opt(dst,src,perm,elt_size,tot_size) \
{\
  int u; \
  for(u=0;u<tot_size;u+=elt_size)  permut_opt(dst+u,src+u,perm,elt_size);  \
}


/* ------------------------------------- */
/*                RECEPTION              */
/* ------------------------------------- */


void test_timeout(int * time_ref,int to,const int MSG)
{
  if (to>=0){
    if ((*time_ref)==0) (*time_ref)=time(NULL);
    if ((((int) time(NULL))-(*time_ref))>to)
      _udx_seterror(MSG);
  }
}

UDX_STATIC int internal_read(UDX_FD fd, _udx_buffer in, const UDX_UI32 size)     
{
  int nb;
  if(_udx_error) return(0);
  if (in->buf_x>in->traite) {
    if(in->traite>0) {
      in->buf_x-=in->traite;
      UDX_MEMCPY(in->buf,in->buf+in->traite,in->buf_x);
      in->traite=0;
    }
  }
  else {
    in->buf_x=0;
    in->traite=0;
  }
  if(_udx_error) return(0);
  nb=in->real_read((char *) (in->buf),in->buf_x,in->buf_size-in->buf_x,fd);
  if(_udx_error) return(0);
  if(nb>0) in->buf_x+=nb;
  else {
    if (_udx_error) _udx_seterror(UDX_READ);
  }
  if (((int)size) < nb) {
    return(((int) size));
  }
  else return(nb);
}


UDX_STATIC int read_n_p_fd(_udx_io_rep *src,UDX_IT *dst, const UDX_UI32 elt_size, const UDX_UI32 tot_size,const UDX_IT * perm) 

{
  int last_time=0;
  int disponibles=0;
  int deja_traites=0,nb=0;
  int reste_a_lire=tot_size;
  if(_udx_error) return(0);
  while ( (_udx_noerror()) && (deja_traites<((int)tot_size))&&(!(nb<0)) ) {
    disponibles=UDX_MIN(src->in->buf_x-src->in->traite,tot_size-deja_traites);
    if(disponibles< ((int)elt_size)) {
      nb=internal_read(src->fd,src->in,reste_a_lire);
      if (nb==0) test_timeout(&last_time,get_read_timeout(),UDX_TIMEOUT_READ);
      else  last_time=((int) time(NULL));
    }
    else {
      if(perm) {
	disponibles-=(disponibles%elt_size);
	permut_tab_opt(dst+deja_traites,src->in->buf+src->in->traite,perm,elt_size,disponibles);
      }
      else {
	UDX_MEMCPY(dst+deja_traites,src->in->buf+src->in->traite,disponibles);
      }
      src->in->traite+=disponibles;
      deja_traites+=disponibles;	
      reste_a_lire-=disponibles;
    }
  }
  if ((nb<0) || (_udx_error)) {
    return(-((int)tot_size-deja_traites)); 
  }
  else return(tot_size);
}

int (*read_n_p)(_udx_io_rep *,UDX_IT *, const UDX_UI32, const UDX_UI32,const UDX_IT *) =read_n_p_fd;


UDX_STATIC int read_1_p_fd(_udx_io_rep *src,UDX_IT *dst, const UDX_UI32 elt_size,const UDX_IT * perm) 

{
  int last_time=0;
  int nb=0;
  if(_udx_error) return(0);
  while ((_udx_noerror()) && ((src->in->buf_x-src->in->traite) < elt_size)) {
    nb=internal_read(src->fd,src->in,elt_size);
    if (nb==0) test_timeout(&last_time,get_read_timeout(),UDX_TIMEOUT_READ);
    else  last_time=((int) time(NULL));
    if(nb<0) {
      return(nb);
    }
  }
  if(_udx_error) return(0);
  if(perm) {
    permut_opt(dst,(src->in->buf+src->in->traite),perm,elt_size);
  }
  else {
    UDX_MEMCPY(dst,(src->in->buf+src->in->traite),elt_size);
  }
  src->in->traite+=elt_size;
  return(elt_size);
}

int (*read_1_p)(_udx_io_rep *,UDX_IT *, const UDX_UI32 ,const UDX_IT * )=read_1_p_fd;


/* ------------------------------------- */
/*                EMISSION               */
/* ------------------------------------- */



int _udx_flush(_udx_io_rep *dst)
{
  int last_time=0;
  int nb_phys_sent=0;
  int boucle_phys_sent=0;
  int tot_nb_tosend=dst->out->buf_x;
  if(_udx_error) return(0);
  while ((_udx_noerror()) && ((tot_nb_tosend>0) && (!(boucle_phys_sent<0)))) {
    boucle_phys_sent=dst->out->real_write((char *) (dst->out->buf),nb_phys_sent,tot_nb_tosend,dst->fd); 
    if (boucle_phys_sent==0) test_timeout(&last_time,get_write_timeout(),UDX_TIMEOUT_WRITE);
    else  last_time=((int) time(NULL));
    if(_udx_noerror()) {
      tot_nb_tosend-=boucle_phys_sent;
      nb_phys_sent+=boucle_phys_sent;
    }
  }
  if(_udx_error) return(0);  
  if(boucle_phys_sent<0) {
    return(-nb_phys_sent);
  }
  else {
    dst->out->buf_x=0;
    return(nb_phys_sent);
  }
}

UDX_STATIC int send_n_p_fd(_udx_io_rep *dst,const UDX_IT *src, const UDX_UI32 elt_size, const UDX_UI32 tot_size,const UDX_IT *perm) {
  int loop_sent=0;
  UDX_UI32 deja_traites=0;
  if(_udx_error) return(0);  
  while ((_udx_noerror()) && ((!(loop_sent<0)) && (deja_traites<tot_size))) {
    loop_sent=UDX_MIN(dst->out->buf_size-dst->out->buf_x,tot_size-deja_traites);
    if (((loop_sent<((int)elt_size)) && perm ) || (loop_sent == 0) ) 
      _udx_flush(dst);
    else {
      if(perm){ 
	loop_sent-=(loop_sent%elt_size);
	permut_tab_opt(dst->out->buf+dst->out->buf_x,src+deja_traites,perm,elt_size,loop_sent);
      }
      else {
	UDX_MEMCPY(dst->out->buf+dst->out->buf_x,src+deja_traites,loop_sent);
      }
      deja_traites+=loop_sent;
      dst->out->buf_x+=loop_sent;	
    }
  }
  if(_udx_error) return(0);  
  if (loop_sent<0) return(-((int)deja_traites));
  else return(deja_traites);
}

int (*send_n_p)(_udx_io_rep *,const UDX_IT *, const UDX_UI32, const UDX_UI32,const UDX_IT *)=send_n_p_fd;

UDX_STATIC int send_1_p_fd(_udx_io_rep *dst,const UDX_IT *src, const UDX_UI32 elt_size,const UDX_IT *perm) {
  if(_udx_error) return(0);  
  if ((dst->out->buf_size-dst->out->buf_x)<elt_size)  _udx_flush(dst);
  if(perm) {
    permut_opt((dst->out->buf+dst->out->buf_x),src,perm,elt_size);
  }
  else {
    UDX_MEMCPY(dst->out->buf+dst->out->buf_x,src,elt_size);
  }
  dst->out->buf_x+=elt_size;	
  if(_udx_error) return(0);  
  return(elt_size);
}

int (*send_1_p)(_udx_io_rep *,const UDX_IT *, const UDX_UI32,const UDX_IT *)=send_1_p_fd;

UDX_STATIC void init__udx_buff(_udx_buffer_rep * b, const unsigned int s)
{
  b->buf=(UDX_IT *) UDX_ALLOC_ATOMIC(s+1);
  b->buf_x=0;
  /*  b->buf+=1;*/
  b->buf_size=s;
  b->traite=0;
}

void init__udx_io(_udx_io_rep *dst, UDX_FD fd, UDX_UI32 r,int (*read_init)(_udx_io_rep *),int (*send_init)(_udx_io_rep *),int (*fct_read) (char *,const int,const int,const int),int (*fct_write) (char *,const int,const int,const int),const UDX_UI32 max_size)
{
  initialise_tableau(dst->tab_externe_to_interne);
  initialise_tableau(dst->tab_interne_to_externe);
  init__udx_buff(dst->in,UDX_DEFAULT_BUF_SIZE);
  init__udx_buff(dst->out,UDX_DEFAULT_BUF_SIZE);
  /*  if(prot_uninitialized) {
      init_udx();
      }*/
  dst->fd=fd;
  dst->in->real_read=fct_read;
  dst->out->real_write=fct_write;
  dst->ready=r;
  dst->received_perm=0;
  dst->sent_perm=0;
  dst->read_init=read_init;
  dst->send_init=send_init;
  dst->max_local_size=max_size;
  dst->max_target_size=max_size;
}

void _udx_send_init(_udx_io_rep *dst)
{
  if (dst->sent_perm==0) {
    dst->sent_perm=1;
    dst->send_init(dst);
   }
}

void _udx_read_init(_udx_io_rep *src)
{
  if (src->received_perm==0) {
    src->received_perm=1;
    src->read_init(src);
  }
}

int   send_1_int  (_udx_io_rep *a, const UDX_IT *b, const UDX_UI32 c)
{
  _udx_send_init(a);
  if ((a->max_target_size)<c) _udx_seterror(UDX_SIZE_WRITE);
  return(send_1_p(a,b,c,a->tab_interne_to_externe[c]));
}

#define send_1_t(a,b,c,d) ((d==c) ? send_1_p(a,b,c,a->tab_interne_to_externe[c]) : 0)

int   send_n_int  (_udx_io_rep *a, const UDX_IT *b, const UDX_UI32 c,
			      const UDX_UI32 d)
{
  _udx_send_init(a);
  if ((a->max_target_size)<c) _udx_seterror(UDX_SIZE_WRITE);
  return(((d>c) ? send_n_p(a,b,c,d,a->tab_interne_to_externe[c]): send_1_t(a,b,c,d)));
}

int   read_1_int  (_udx_io_rep *a, UDX_IT *b, const UDX_UI32 c)
{
  _udx_read_init(a);
  if ((a->max_local_size)<c) _udx_seterror(UDX_SIZE_READ);
  return(read_1_p(a,b,c,a->tab_externe_to_interne[c]));
}

#define read_1_t(a,b,c,d) ((d==c) ? read_1_p(a,b,c,a->tab_externe_to_interne[c]) : 0)


int   read_n_int  (_udx_io_rep *a, UDX_IT *b, const UDX_UI32 c,
			      const UDX_UI32 d)
{
  _udx_read_init(a);
  if ((a->max_local_size)<c) _udx_seterror(UDX_SIZE_READ);
  return(((d>c) ? read_n_p(a,b,c,d,a->tab_externe_to_interne[c]): read_1_t(a,b,c,d)));
}
