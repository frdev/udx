#ifndef __udx_io_h
#define __udx_io_h

#include "udx_perm.h"
#include "udx_error.h"

/* extern void (*init_udx)(void); */

/* ********************************************************************* */
/*      PARAMETRES                                                       */
/* ********************************************************************* */
/* 

   l'utilisateur doit fournir des pointeurs sur des fonctions lecture
   ecriture physiques

   (*real_write) (char *tab,const int offset,const int nb,const int fd);

   (*real_read)  (char *,const int,const int,const int);


   * real_write prend "nb" elements du tableau "tab" a partir du rang 
                "offset" et essaye de les ecrire sur le support physique 
                reference par l'entier "fd" (en general file descriptor).
	        la fonction retourne le nombre de caracteres effectivement 
                ecrits.

   * real_read  essaye de lire "nb" elements et les stocke dans le  
                tableau "tab" a partir du rang "offset" sur le support 
                physique reference par l'entier "fd" 
		(en general file descriptor).
                la fonction retourne le nombre de caracteres 
                effectivement lus.
*/

/* ********************************************************************* */
/*      STRUCTURE DE DONNEES BUFFERS                                     */
/* ********************************************************************* */

typedef struct {
  UDX_IT *buf;         /* buffer lec/ecriture */
  UDX_UI32  buf_size;     /* La taille du buffer */
  UDX_UI32  buf_x;        /* position reelle dans le buffer*/
  UDX_UI32  traite;       /* les octets deja traites */
  int     (*real_write) (char *,const int,const int,const int);  
  int     (*real_read)  (char *,const int,const int,const int);  
} _udx_buffer_rep;

typedef _udx_buffer_rep _udx_buffer[1];

/* ********************************************************************* */
/*      STRUCTURE DE DONNEES BUFFERS UDX                                 */
/* ********************************************************************* */

typedef struct _udx_io_rep {
  UDX_FD fd;                                        /* file desc */
  UDX_IT *tab_externe_to_interne[UDX_BIG_SIZE];     
  UDX_IT *tab_interne_to_externe[UDX_BIG_SIZE]; 
  _udx_buffer in;                  /* buffer en lecture */
  _udx_buffer out;                 /* buffer en ecriture */
  UDX_UI32 ready;
  UDX_UI32 received_perm;
  UDX_UI32 sent_perm;
  int (*read_init)(struct _udx_io_rep *);
  int (*send_init)(struct _udx_io_rep *);
  UDX_UI32 max_target_size;
  UDX_UI32 max_local_size;
} _udx_io_rep;

/* typedef _udx_io_rep _udx_io[1]; */


/* ********************************************************************* */
/*      ECRITURE fonctions principales                                   */
/* ********************************************************************* */

extern  int   (*send_n_p) (_udx_io_rep *, const UDX_IT *, const UDX_UI32 ,
			   const UDX_UI32 ,const UDX_IT *);
extern  int   (*send_1_p) (_udx_io_rep *, const UDX_IT *, const UDX_UI32 ,
			   const UDX_IT *);

/* ********************************************************************* */
/*      LECTURE fonctions principales                                    */
/* ********************************************************************* */

extern  int   (*read_n_p) (_udx_io_rep *, UDX_IT *, const UDX_UI32 ,
			   const UDX_UI32,const UDX_IT *);
extern  int   (*read_1_p) (_udx_io_rep *, UDX_IT *, const UDX_UI32 ,
			   const UDX_IT *);

/* ********************************************************************* */
/*      FLUSH                                                            */
/* ********************************************************************* */

extern int _udx_flush(_udx_io_rep *);

/* ********************************************************************* */
/*      Initialisation channel                                           */
/* ********************************************************************* */

extern void init__udx_io (_udx_io_rep *,UDX_FD ,UDX_UI32, 
			 int (*read_init)(_udx_io_rep *),
			 int (*send_init)(_udx_io_rep *),
			 int (*fct_read) (char *,const int,const int,const int),
			 int (*fct_write) (char *,const int,const int,const int),
			 const UDX_UI32);

/* ********************************************************************* */
/*      taille buffers internes                                          */
/* ********************************************************************* */

extern unsigned int UDX_DEFAULT_BUF_SIZE;

/* ********************************************************************* */
/*      Interface utilisateur                                            */
/* ********************************************************************* */

extern  int   send_1_int  (_udx_io_rep *, const UDX_IT *, const UDX_UI32);
extern  int   send_n_int  (_udx_io_rep *, const UDX_IT *, const UDX_UI32 ,
			   const UDX_UI32);
extern  int   read_1_int  (_udx_io_rep *, UDX_IT *, const UDX_UI32);
extern  int   read_n_int  (_udx_io_rep *, UDX_IT *, const UDX_UI32 ,
			   const UDX_UI32);
extern void  _udx_read_init(_udx_io_rep *);
extern void  _udx_send_init(_udx_io_rep *);

#endif /* __udx_h */
