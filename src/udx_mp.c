#include "udx_mp.h"
#include <stdio.h>
#include <stdlib.h>

static int read_i_n_int(_udx_io_rep * can,UDX_IT *t,const UDX_UI32 s,const UDX_UI32 st)
{
  int i=0;
  /*  debug("read_i_n_int %d %d ",s,st);*/
  for(i=(st-s);i>=0;i-=s) read_n_int(can,t+i,s,s);
  return(1);
}

static int send_i_n_int(_udx_io_rep * can,const UDX_IT *t,const UDX_UI32 s,const UDX_UI32 st)
{
  int i=0;
  for(i=st-s;i>=0;i-=s) send_n_int(can,t+i,s,s);
  return(1);
}

int read_mp_int(_udx_io_rep * can,_udx_mp_rep * dst,UDX_IT *t,UDX_UI32 *s)
{
  if (dst->l_first) {
    return(read_n_int(can,t,dst->e_limb,(*s)*(dst->e_limb)));
  }
  else {
    return(read_i_n_int(can,t,dst->e_limb,(*s)*(dst->e_limb)));
  }
}

int send_mp_int(_udx_io_rep * can,_udx_mp_rep * dst,const UDX_IT * t,const UDX_UI32 s)
{
  /*  debug("send_mp_int %d ",s);*/
  if (dst->l_first) return(send_n_int(can,t,dst->s_limb,s*(dst->e_limb)));
  else return(send_i_n_int(can,t,dst->s_limb,s*(dst->e_limb)));
}

int (*read_mp)(_udx_io_rep *,_udx_mp_rep *,UDX_IT *,UDX_UI32 *)=read_mp_int;

int (*send_mp)(_udx_io_rep *,_udx_mp_rep *,const UDX_IT *,const UDX_UI32)=send_mp_int;

int new__udx_mp(_udx_io_rep *c,_udx_mp_rep *cg,const UDX_UI32 sl,const UDX_UI32 sl2,const UDX_UI32 o)
{
  cg->l_first=0;
  cg->s_limb=sl;
  cg->e_limb=sl2;
  if ((cg->s_limb) == (cg->e_limb)){
    fprintf(stderr, "\nUse int for gmp");
    read_mp=read_mp_int;
    send_mp=send_mp_int;
  }
  else {
    fprintf(stderr, "\nObsolete use of long in mp");
    abort();
  }
  return(1);
}
