#ifndef __udx_mp_h
#define __udx_mp_h
/* 
   PARI :
   BITS_IN_LONG
   BYTES_IN_LONG
   ca va etre tres simple en utilisant _udx_long.
   on fixe le format externe pour les mp_limbs a 

   * _udx_long_l_first si les 2 machines n'ont pas la meme
   taille de limbs et on fixe max_int a la plus petite 
   des 2 valeurs.
   * _udx_long_perm si les 2 machines ont la meme taille 
   de limbs et on fixe max_int a la valeur commune.

   donc a ce moment on sait envoyer/rececvoir correctement 
   un limb (decoupe en entiers de taille max_int).
   
   Il faut gerer correctement l'envoi du header (tailles) 
   ne pas oublier de multiplier les tailles par 
   sizeof(limb)/sizeof(max_int)
   
   maintenant pour l'entier lui-meme, il y a 2 possibilites
   soit il est code poids faible en tete (mp) soit il es code
   poids fort en tete (pari).
*/

#include "udx_def.h"
#include "udx_error.h"
#include "udx_perm.h"
#include "udx_io.h"

#define _udx_gmp_order 1

#define _udx_pari_order 0


typedef struct {
  UDX_UI32   l_first; /* vaut 1 si low is first 0 sinon  */
  UDX_UI32   s_limb;  /* taille d'un limb format interne */
  UDX_UI32   e_limb;  /* taille d'un limb format externe */
}_udx_mp_rep;

extern int new__udx_mp(_udx_io_rep *,_udx_mp_rep *,const UDX_UI32, const UDX_UI32,const UDX_UI32);
extern int (*read_mp)(_udx_io_rep *,_udx_mp_rep *,UDX_IT *,UDX_UI32 *);
extern int (*send_mp)(_udx_io_rep *,_udx_mp_rep *,const UDX_IT *,const UDX_UI32);

#endif
