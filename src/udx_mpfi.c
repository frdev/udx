#include "udx_mpfi.h"


int udx_write_mpfi(_udx_stream_rep *c, mpfi_srcptr tmp1) {
  // Send the precision, sign, and exponent
    udx_write_mpfr(c, &tmp1->left);
    udx_write_mpfr(c, &tmp1->right);
    return 1; 
}

int udx_read_mpfi(_udx_stream_rep *stream, mpfi_srcptr tmp) {
      // Clear the existing MPFR structure
 
    udx_read_mpfr(stream, &tmp->left);
    udx_read_mpfr(stream, &tmp->right);
    return 1;  
}
