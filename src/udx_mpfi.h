#ifndef _udx_mpfi_h  
#define _udx_mpfi_h
#include "udx_mpfr.h"
#include "mpfi.h"

extern int udx_write_mpfi(_udx_stream_rep *c,mpfi_srcptr tmp1);
extern int udx_read_mpfi(_udx_stream_rep *c,mpfi_srcptr tmp1);

#endif