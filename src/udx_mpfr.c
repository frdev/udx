#include "udx_mpfr.h"


int udx_write_mpfr(_udx_stream_rep *c, mpfr_srcptr tmp1) {
  // Send the precision, sign, and exponent
    send_n_int(&((c)->i), (UDX_IT *)(&(tmp1->_mpfr_prec)), sizeof(mpfr_prec_t), sizeof(mpfr_prec_t));
    send_i32(c, (UDX_IT *)(&(tmp1->_mpfr_sign)));  // Send sign
    send_n_int(&((c)->i), (UDX_IT *)(&(tmp1->_mpfr_exp)), sizeof(mpfr_exp_t), sizeof(mpfr_exp_t)); // Send exponent
    

    int num_limbs = (tmp1->_mpfr_prec + GMP_LIMB_BITS - 1) / GMP_LIMB_BITS; // Calculate number of limbs needed
    send_n_int(&((c)->i), (UDX_IT *)(tmp1->_mpfr_d), 8, 8 * num_limbs); // Send the limbs

    return 1; 
}

int udx_read_mpfr(_udx_stream_rep *stream, mpfr_srcptr tmp) {
    // Clear the existing MPFR structure
    //mpfr_clear(tmp);  // if I clear the structure, if it is not initialised in the same process, it will crash
    long tmp_prec;
   
    // Read precision, sign, and exponent
    read_n_int(&((stream)->i), (UDX_IT *)(&(tmp_prec)), sizeof(mpfr_prec_t), sizeof(mpfr_prec_t));
    // Allocate memory for limbs
    mpfr_init2(tmp, tmp_prec);
   
    read_i32(stream, (UDX_IT *)(&(tmp->_mpfr_sign)));  // Read sign
    read_n_int(&((stream)->i), (UDX_IT *)(&(tmp->_mpfr_exp)), sizeof(mpfr_exp_t), sizeof(mpfr_exp_t)); // Read exponent

    // Calculate the number of limbs needed based on precision
    int num_limbs = (tmp->_mpfr_prec + GMP_LIMB_BITS - 1) / GMP_LIMB_BITS;

    // Read the limbs
    read_n_int(&((stream)->i), (UDX_IT *)(tmp->_mpfr_d), 8, 8 * num_limbs);

    return 1; 
}
