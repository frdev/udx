#ifndef _udx_mpfr_h  
#define _udx_mpfr_h
#include "udx_stream.h"
#include "mpfr.h"

extern int udx_write_mpfr(_udx_stream_rep *c,mpfr_srcptr tmp1);
extern int udx_read_mpfr(_udx_stream_rep *c,mpfr_srcptr tmp1);

#endif