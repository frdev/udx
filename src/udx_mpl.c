#include "udx_mpl.h"

static MKernelVector kv;

ALGEB M_DECL mudx_init_run_tcp_client(MKernelVector kv, ALGEB *args)
{
    void *ch = udx_init_run_tcp_client(MapleToString(kv, args[1]),
                                       MapleToString(kv, args[2]), MapleToString(kv, args[3]), MapleToString(kv, args[4]), MapleToInteger32(kv, args[5]));
    return (ToMaplePointer(kv, (void *)ch, (M_INT)0));
}

ALGEB M_DECL mudx_init_tcp_server_negoc(MKernelVector kv, ALGEB *args)
{
    void *ch = udx_init_tcp_server_negoc(MapleToString(kv, args[1]),
                                         MapleToInteger32(kv, args[2]), MapleToInteger32(kv, args[3]));
    return (ToMaplePointer(kv, (void *)ch, (M_INT)0));
}

ALGEB M_DECL mudx_init_tcp_client_negoc(MKernelVector kv, ALGEB *args)
{

    void *ch = udx_init_tcp_client_negoc(MapleToString(kv, args[1]),
                                         MapleToInteger32(kv, args[2]), MapleToInteger32(kv, args[3]));
    return (ToMaplePointer(kv, (void *)ch, (M_INT)0));
}

ALGEB M_DECL mudx_read_i32(MKernelVector kv, ALGEB *args)
{
    int n = 0;
    udx_read_i32(MapleToPointer(kv, args[1]), &n);
    printf("n: %d\n", n);
    return (ToMapleInteger(kv, n));
}

ALGEB M_DECL mudx_write_i32(MKernelVector kv, ALGEB *args)
{
    int n = MapleToInteger32(kv, args[2]);
    udx_write_i32(MapleToPointer(kv, args[1]), &n);
    return (ToMapleInteger(kv, 1));
}

ALGEB M_DECL mudx_flush(MKernelVector kv, ALGEB *args)
{
    udx_flush(MapleToPointer(kv, args[1]));
    return (ToMapleInteger(kv, 1));
}

ALGEB M_DECL mudx_read_mpz(MKernelVector kv, ALGEB *args)
{
    mpz_t  n;
    udx_read_mpz(MapleToPointer(kv, args[1]), n);
    return (GMPIntegerToMaple(kv, n));
}

ALGEB M_DECL mudx_write_mpz(MKernelVector kv, ALGEB *args)
{
    udx_write_mpz(MapleToPointer(kv, args[1]), MapleToGMPInteger(kv, args[2]));
    return (ToMapleInteger(kv, 1));
}


ALGEB M_DECL mudx_write_sfloat_to_mpfr(MKernelVector kv, ALGEB *args) 
{
    // Type checking - ensure input is a Maple float
 
    
    // Extract mantissa and exponent from SFloat
    // SFloat in Maple is stored as mantissa * 10^exponent
    // SFloatMantissa and SFloatExponent are Maple functions that extract these parts
    ALGEB mantissa = EvalMapleProc(kv, ToMapleName(kv, "SFloatMantissa", TRUE), 1, args[2]);
    ALGEB exponent = EvalMapleProc(kv, ToMapleName(kv, "SFloatExponent", TRUE), 1, args[2]);
    
    // Initialize MPFR number
    mpfr_t result;
    mpfr_init(result);  // Uses default precision
    
    // Conversion process:
    // 1. Set result to exponent value
    mpfr_set_z(result, MapleToGMPInteger(kv, exponent), MPFR_RNDN);
    
    // 2. Compute 10^exponent
    mpfr_exp10(result, result, MPFR_RNDN);
    
    // 3. Multiply by mantissa to get final value: mantissa * 10^exponent
    mpfr_mul_z(result, result, MapleToGMPInteger(kv, mantissa), MPFR_RNDN);

    udx_write_mpfr(MapleToPointer(kv, args[1]), result);
    return (ToMapleInteger(kv, 1));  
}

ALGEB M_DECL mudx_read_mpfr_to_sfloat(MKernelVector kv, ALGEB *args)
{
    mpfr_t result;
    udx_read_mpfr(MapleToPointer(kv, args[1]), result);

    mpz_t ma;
    mpz_init(ma);
    int exp2 = mpfr_get_z_2exp(ma, result);
    // TODO verfy 
    return(ToMapleExpressionSequence(kv, 2,
				       GMPIntegerToMaple(kv,ma), ToMapleInteger(kv,exp2)));
}


