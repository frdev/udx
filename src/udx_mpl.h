#ifndef _udx_mpl_h
#define _udx_mpl_h


#include "udx.h"
#include "udx_mpfr.h"
#include <gmp.h>
#include <mpfr.h>
// gmp.h has to be included before maplec.h !!
#include "maplec.h"
#include <stdio.h>

ALGEB M_DECL mudx_init_run_tcp_client(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_init_tcp_server_negoc(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_init_tcp_client_negoc(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_i32(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_write_i32(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_flush(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_mpz(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_write_mpz(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_write_string(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_string(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_mpfr(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_mpfi(MKernelVector kv, ALGEB *args);

ALGEB M_DECL mudx_write_sfloat_to_mpfr(MKernelVector kv, ALGEB *args);
ALGEB M_DECL mudx_read_mpfr_to_sfloat(MKernelVector kv, ALGEB *args);



#endif