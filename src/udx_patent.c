#include "udx_patent.h"

#define XDR_TO_MATH "\1\0\2\0\1\4\0\1\2\3"
#define XDR_TO_MATH_LONG 3

UDX_STATIC UDX_UI32 uninitialized=1;
UDX_STATIC const UDX_IT xdr_to_math[]=XDR_TO_MATH;

/*******************************************************
		  Entiers magiques
********************************************************/

#define genere_entier_magique(typ) \
UDX_STATIC typ entier_magique_ ## typ (const int sa) { \
 typ a=0; \
 int c; \
  for (c=0;c<sa;c++) { \
      a<<=8;           \
      a+=c;            \
  }                    \
  return a;            \
}

#define genere_i2m(typ) \
UDX_STATIC UDX_IT *cons_i2m_ ## typ (const typ a) { \
  UDX_IT *tmp=(UDX_IT *)UDX_ALLOC_ATOMIC(UDX_SIZEOF(a)); \
  UDX_MEMCPY((void *)tmp, (void *)(&a), UDX_SIZEOF(a)); \
  return tmp; \
}

/*----------------------------------------*/
/* initialisation du protocole            */
/*----------------------------------------*/

#define genere_prot_init(typ) \
UDX_STATIC_INLINE void prot_init_ ## typ (UDX_IT *my_permut, UDX_UI32 *rang) { \
  UDX_UI32 size; \
  typ a; \
  size=UDX_SIZEOF(a); \
  if (_udx_local_infos->tab_interne_to_math[size]==UDX_PTR_NULL) {\
    a=entier_magique_ ## typ (size); \
    _udx_local_infos->tab_interne_to_math[size]=cons_i2m_ ## typ (a); \
    _udx_local_infos->tab_math_to_interne[size]=inv_permut(_udx_local_infos->tab_interne_to_math[size],size);   \
    my_permut[0]++; \
    my_permut[(*rang)++]=size; \
    UDX_MEMCPY((void *)(my_permut+(*rang)), (void *)_udx_local_infos->tab_interne_to_math[size], size); \
    (*rang)+=size; \
  }\
}

/* mon codage interne des differents types. */
/* Son format :
   my_permut[0]= le nombre de permutation
   si ==0, on demande a l'autre ses permuttations, 
           et c'est nous qui les effectuerons
           a l'emission et a la reception
   my_permut[1]= nb elt dans la premiere permut
   my_permut[2 -> my_permut[1]]= les elts.
   etc...

*********************************************/
UDX_STATIC_INLINE UDX_IT *init_my_permut () {
  UDX_IT *my_permut=(UDX_IT *)UDX_ALLOC_ATOMIC(UDX_BIG_SIZE);
  my_permut[0]=0;
  return my_permut;
}

/* fabrice enleve les ";" qui ne sont pas "ansi" */

#define genere_permut(typ) \
     genere_entier_magique(typ) \
     genere_i2m(typ) \
     genere_prot_init(typ) 

/***********************************/
/* On genere les permutation pour  
   les types existants.
   On considere que le plus grand
   est le long                     */
/***********************************/

/* fabrice enleve les ; (pas ansi) */
/* genere_permut(char) */
#define entier_magique_char(sa) 0
genere_i2m(char)					\
genere_prot_init(char) 

genere_permut(short)
genere_permut(int)
genere_permut(long)

void full_init_udx()
{

  if(uninitialized) {
    UDX_UI32 i,rang=0,ptr=0;
    
    _udx_local_infos->local_permut=UDX_PTR_NULL;
    _udx_local_infos->local_permut=init_my_permut();
    prot_init_char(_udx_local_infos->local_permut, &_udx_local_infos->size_local_permut);
    prot_init_short(_udx_local_infos->local_permut,&_udx_local_infos->size_local_permut);
    prot_init_int(_udx_local_infos->local_permut, &_udx_local_infos->size_local_permut);
    prot_init_long(_udx_local_infos->local_permut, &_udx_local_infos->size_local_permut);

    /* initialisation lecture xdr */

    initialise_tableau(_udx_local_infos->tab_xdr_to_interne);
    initialise_tableau(_udx_local_infos->tab_interne_to_xdr);
    
    for(i=1;i<=XDR_TO_MATH_LONG;i++) {
      rang=xdr_to_math[ptr];
      ptr++;
      
      if (UDX_MEMCMP(xdr_to_math+ptr,_udx_local_infos->tab_math_to_interne[rang],rang)!=0) {
	/*	debug("codage interne <> xdr pour %d\n",rang); */
	_udx_local_infos->tab_xdr_to_interne[rang]=(UDX_IT *)UDX_ALLOC_ATOMIC(rang);
	_udx_local_infos->tab_interne_to_xdr[rang]=(UDX_IT *)UDX_ALLOC_ATOMIC(rang);
	UDX_MEMCPY(_udx_local_infos->tab_xdr_to_interne[rang],comp_permut((UDX_IT *)xdr_to_math+ptr,_udx_local_infos->tab_math_to_interne[rang],rang),rang);
	UDX_MEMCPY(_udx_local_infos->tab_interne_to_xdr[rang],inv_permut(_udx_local_infos->tab_xdr_to_interne[rang],rang),rang);
      } 
      else {
	/*	debug("codage interne = xdr pour %d\n",rang); */
	_udx_local_infos->tab_xdr_to_interne[rang]=UDX_PTR_NULL;
	_udx_local_infos->tab_interne_to_xdr[rang]=UDX_PTR_NULL;
      }
      ptr+=rang;
    }
    uninitialized=0;
  }
/*  return(&(_udx_local_infos));*/
}


typedef unsigned int _udx_uint;
typedef unsigned short _udx_short;

int detect_ieee_patent()
{
  double a=-2;
  if (((UDX_IT*)(&a))[0]) 
    return(1);
  else return(0);
}


int detect_long_patent()
{
  long a=1;
  if (((_udx_uint *)(&a))[0])
    return(1);
  else return(0);
}
