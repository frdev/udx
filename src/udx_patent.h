#ifndef __udx_patent_h
#define __udx_patent_h

#include "udx_perm.h"

extern void full_init_udx(void);
extern int  detect_ieee_patent(void);
extern int  detect_long_patent(void);

#endif
