/***************


	  UDX   Universal Data eXchange


**************/


/****************************************************************
 Definition des permutations,
****************************************************************/

#include "udx_perm.h"


_udx_infos _udx_local_infos;


/********************************************************/
/********************************************************/
/* permutation math to interne            */
/* (connaissant interne to math)          */
/* c'est la permutation inverse generique */

UDX_IT *inv_permut (const UDX_IT *a, const UDX_UI32 sa) {
  UDX_IT *tmp=UDX_PTR_NULL;
  UDX_UI32 c;
  if (a) {
    tmp=(UDX_IT *)UDX_ALLOC_ATOMIC(sa);
    for (c=0;c<sa;c++)    tmp[a[c]]=c;
  }
  return tmp;
}


/********************************************************/
/********************************************************/
/*-------On compose la permutation composee ------------------*/
UDX_IT *comp_permut (const UDX_IT *a, const UDX_IT *b, const UDX_UI32 sa) {
  UDX_IT *tmp=(UDX_IT *)UDX_ALLOC_ATOMIC(sa);
  UDX_UI32 c;
  for (c=0;c<sa;c++)    tmp[c]=b[a[c]];
  return tmp;
}

/********************************************************/
/********************************************************/
/*----------------------------------------*/
#define UDX_TEST 1
void permut (UDX_IT *dest, const UDX_IT *src, const UDX_IT *perm, const UDX_UI32 sa) {
  UDX_UI32 c;
  for (c=0;c<sa;c++)     {
    dest[c]=src[perm[c]];
  }
}

/********************************************************/
/********************************************************/
/* initialise les tableaux des permutations avec
   0 pour chaque valeur
*/
void initialise_tableau (UDX_IT **tab) {
  UDX_UI32 i;
  for (i=0;i<UDX_BIG_SIZE;i++) 
    tab[i]=UDX_PTR_NULL;
}

/*
  Permute un tableau
*/
UDX_UI32 permut_tab(UDX_IT *dst, UDX_IT *src, const UDX_IT *perm, const UDX_UI32 elt_size, const UDX_UI32 tot_size) {
  UDX_UI32 i=0;
  UDX_UI32 start=0;

  /* lecture invariante */
  if (perm==UDX_PTR_NULL) {
    UDX_MEMCPY(dst,src,tot_size);
    return(tot_size);
  }

  /* On permute le tableau */
  while(i<(tot_size)) {
    permut(dst+start+i,src+start+i,perm,elt_size);  
    i+=elt_size;
  }

  if (i>(tot_size)) i-=elt_size;

  return i;
}

