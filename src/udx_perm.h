#ifndef ___udx_perm_h
#define ___udx_perm_h
 
#include "udx_def.h"

/* ******************************************************************** */
/* ******************************************************************** */
/*            Structure de donnes                                      */
/* ******************************************************************** */

typedef struct {
  UDX_IT *tab_math_to_interne[UDX_BIG_SIZE];
  UDX_IT *tab_interne_to_math[UDX_BIG_SIZE];
  UDX_IT *tab_xdr_to_interne[UDX_BIG_SIZE];
  UDX_IT *tab_interne_to_xdr[UDX_BIG_SIZE];
  UDX_IT *local_permut;
  UDX_UI32  size_local_permut;
} _udx_infos_rep;

typedef _udx_infos_rep _udx_infos[1];

extern _udx_infos _udx_local_infos;

/* ******************************************************************** */
/*            Gesttion de permutations                                  */
/* ******************************************************************** */

/* Composition de 2 permutations */

extern UDX_IT * comp_permut        (const UDX_IT *a, const UDX_IT *b, 
				     const UDX_UI32 sa);

/*  Calcul d'une permutation */

extern void      permut             (UDX_IT *dest, const UDX_IT *src, 
				     const UDX_IT *perm, const UDX_UI32 sa);
/*  Reciproque d'une permutation */

extern UDX_IT * inv_permut         (const UDX_IT *, const UDX_UI32 );

/* Initialisation d'un tableau de permutations */
extern void      initialise_tableau (UDX_IT **tab);

/* Permutation d'un tableau */
extern UDX_UI32    permut_tab         (UDX_IT *dst, UDX_IT *src, 
				     const UDX_IT *perm, 
				     const UDX_UI32 elt_size, 
				     const UDX_UI32 tot_size);

/* ******************************************************************** */
/*            COMPATIBILITE XDR                                         */
/* ******************************************************************** */

#define UDX_HTONL(a,b) if (_udx_local_infos->tab_interne_to_xdr[4]!=UDX_PTR_NULL) \
                       permut((UDX_IT *)&a,(UDX_IT *)&b,_udx_local_infos->tab_interne_to_xdr[4],4); \
                       else \
                       UDX_MEMCPY(&a,&b,4);
#define UDX_NTOHL(a,b) if (_udx_local_infos->tab_xdr_to_interne[4]!=UDX_PTR_NULL) \
                       permut((UDX_IT *)&a,(UDX_IT *)&b,_udx_local_infos->tab_xdr_to_interne[4],4); \
                       else \
                       UDX_MEMCPY(&a,&b,4);
#define UDX_HTONS(a,b) if (_udx_local_infos->tab_interne_to_xdr[2]!=UDX_PTR_NULL) \
                       permut((UDX_IT *)&a,(UDX_IT *)&b,_udx_local_infos->tab_interne_to_xdr[2],2); \
                       else \
                       UDX_MEMCPY(&a,&b,2);
#define UDX_NTOHS(a,b) if (_udx_local_infos->tab_xdr_to_interne[2]!=UDX_PTR_NULL) \
                       permut((UDX_IT *)&a,(UDX_IT *)&b,_udx_local_infos->tab_xdr_to_interne[2],2); \
                       else \
                       UDX_MEMCPY(&a,&b,2);


#endif /* __udx_perm_h */
