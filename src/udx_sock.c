#ifndef NO_UDX_SOCK
#include "udx_sock.h"
#include "udx_error.h"
#ifndef WIN32
#ifndef __CYGWIN32__
#include <netinet/tcp.h>
#else
#include <cygwin/socket.h>
#endif
#endif
  /* 
     creation de sockets :
       si on met le port a 0 un numero de port sera affecte 
       lors du bind
       c'est le moyen de trouver un port libre attention il faut 
       faire ntohs dessus. 
  */
#ifndef  TTSOCKBUF_W
#define  TTSOCKBUF_W 0x8000
#endif
#ifndef  TTSOCKBUF_R
#define  TTSOCKBUF_R 0x8000
#endif

void _udx_set_sock_opt(int unew)
{
  int i,j;
#ifdef WIN32
  u_long FAR li=0;
#endif
#ifndef UDX_NO_NODELAY
  i=1; j=sizeof(int);
  setsockopt(unew,IPPROTO_TCP, TCP_NODELAY,(char *)(&i),j);
#ifdef WIN32
  getsockopt(unew,SOL_SOCKET , SO_RCVBUF,(void *)(&i),(&j));
  /*  fprintf(stderr,"\n RCV : %d",i);*/
  getsockopt(unew,SOL_SOCKET , SO_SNDBUF ,(void *)(&i),(&j));
  /*  fprintf(stderr,"\n SND : %d\n",i);*/
  ioctlsocket(unew,FIONBIO,&li);
#else
  getsockopt(unew,SOL_SOCKET , SO_RCVBUF,(void *)(&i),(socklen_t *)(&j));
  /*  fprintf(stderr,"\n RCV : %d",i);*/
  getsockopt(unew,SOL_SOCKET , SO_SNDBUF ,(void *)(&i),(socklen_t *)(&j));
  /*  fprintf(stderr,"\n SND : %d\n",i);*/
  fcntl(unew,F_SETFL,O_NONBLOCK);
#endif
#endif
}

int accept_sock(int usock,struct sockaddr_in *corresp,struct timeval * toto)
{
  unsigned int _udx_size=0;
  int unew=0,res_sel=0;
  struct timeval to=*toto; 
  fd_set active_fd_set;
  FD_ZERO (&active_fd_set);
  FD_SET ((unsigned int) usock, &active_fd_set);
  if ((res_sel=select (usock+1, &active_fd_set, NULL, NULL, &to)) <= 0)
    {
      if (res_sel == 0) _udx_seterror(UDX_TIMEOUT_CONNECT);
      else _udx_seterror(UDX_SELECT_SOCKET);
      return (-1);
    }
  _udx_size = sizeof ((*corresp));
#ifndef WIN32
  unew = accept (usock,(struct sockaddr *)(corresp),(void *)(&_udx_size)); 
#else
  unew = accept (usock, (struct sockaddr *)(corresp),(int *)(&_udx_size));
#endif
  close(usock);
  if (unew < 0)
    {
      _udx_seterror(UDX_ACCEPT);
      return -1;
    }
  _udx_set_sock_opt(unew);
  FD_SET ((unsigned int)unew, &active_fd_set);
  return(unew);
}

#ifdef WIN32
static int win_sock_init=0;
 
int init_win_sock()
{
        int err=0;
        WORD wVersionRequested;
        WSADATA wsaData;
        wVersionRequested = MAKEWORD( 2, 0 );
	err=WSAStartup( wVersionRequested, &wsaData );
        if(err==0) return(err); 
        wVersionRequested = MAKEWORD( 1, 1 );
	err=WSAStartup( wVersionRequested, &wsaData );
        if(err==0) return(err); 
        wVersionRequested = MAKEWORD( 1, 0 );
	err=WSAStartup( wVersionRequested, &wsaData );
        if(err==0) return(err); 
        else {
		  _udx_seterror(UDX_WINSOCK_ERROR);
		  return(-1);
        }
}
#endif 


int open_sock_server (const UDX_STRING corresname,const UDX_UI16 port, const UDX_UI16 sock_mode, 
		      const UDX_UI16 options, struct sockaddr_in *corresp,struct timeval *to) {

  int usock=0, unew=0;
  unsigned int length=0;
  struct sockaddr_in name;
#ifdef WIN32
  if(win_sock_init==0) {
	  if ((init_win_sock())!=0) {
	    _udx_seterror(UDX_WINSOCK_ERROR);
       	    return(-1);
	  }
	  else win_sock_init=1;
  }
#endif
  usock = socket (AF_INET, sock_mode, 0);
  if (usock < 0)
    {
         _udx_seterror(UDX_SOCKET);
         return -1;
    }
  name.sin_family = AF_INET;
  name.sin_port = htons (port);
  name.sin_addr.s_addr = 0;
  if (bind (usock, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
         _udx_seterror(UDX_BIND);
         return -1;
    }
  if((port)==0) {
    length=sizeof(name);
#ifndef WIN32
    if (getsockname(usock,(struct sockaddr*)(&name),(void *)(&length))<0)
#else
    if (getsockname(usock,(struct sockaddr*)(&name),(int *)(&length))<0)
#endif
	{
	_udx_seterror(UDX_SOCKNAME);
        return (-1);
      }
    corresp->sin_port=name.sin_port;
    unew=usock;
  }
  if (listen (usock, 1) < 0)
    {
      _udx_seterror(UDX_LISTEN);
      return -1;
    }
  if(unew==0) {
    unew=accept_sock(usock,corresp,to);  
    if (unew < 0)
      {
	_udx_seterror(UDX_ACCEPT);
	return -1;
      }
  }
  return(unew);                                                              }

/* --------------------------------------------- */
/* ouverture d'un client unicast, tcp            */
/* --------------------------------------------- */

int open_sock_client (const UDX_STRING corresname,  UDX_UI16 port, const UDX_UI16 sock_mode, 
		      const UDX_UI16 options, struct sockaddr_in *corresp) {
  int usock;
  struct hostent *ent;            /* notre corresp */
#ifdef WIN32
  if(win_sock_init==0) {
		if ((init_win_sock())!=0) return(-1);
		else win_sock_init=1;
  }
#endif
 
  usock = socket (AF_INET,  sock_mode, 0);
  if (usock < 0) {
    _udx_seterror(UDX_SOCKET);
    return -1;
  }
  ent=gethostbyname(corresname);
  if (ent == NULL)
    {
      _udx_seterror(UDX_UNKNOWN_HOST);
      return -1;
    }
  corresp->sin_family = AF_INET;
  corresp->sin_port = htons (port);
  corresp->sin_addr = *(struct in_addr *)(ent->h_addr);
  
  /* On le connecte */
  if (connect(usock,(struct sockaddr *)corresp,sizeof ((*corresp))))
    {
      _udx_seterror(UDX_CONNECT);
      return -1;
    }
  _udx_set_sock_opt(usock);
  return(usock);
}


static int something_to_read_sock(int fd)
{
  struct timeval to;
  struct timeval * to_ptr;
  fd_set readfds;
  int res=0;
  FD_ZERO(&readfds);
  FD_SET((unsigned int)fd,&readfds);
  if (get_read_timeout()>-1) {  
    to.tv_sec=get_read_timeout();
    to.tv_usec=0;
    to_ptr=&to;
  }
  else {
    to_ptr=NULL;
  }
  if ((res=select(fd+1,&readfds,0,0,to_ptr)) == -1)
    {
      _udx_seterror(UDX_SELECT);
      return -1;
    }
  if (FD_ISSET(fd,&readfds)) {
    return(1);
  }
  _udx_seterror(UDX_TIMEOUT_READ);
  return(0);
}


int real_read_sock(char * out1,const int ofset,const int to_send,const int fd) {
  int res = 0;
  while ((_udx_noerror()) && ((res=something_to_read_sock(fd))<1)) {};
  if(_udx_error) {
    return(0);
  }
  res=recv(fd,(char *) (out1 + ofset),to_send,0);
  /*
#ifdef WIN32
  fprintf(stderr,"/%d,%d/",to_send-res,res);
#else
  fprintf(stderr,"[%d,%d]",to_send-res,res);
#endif
  */
  if (res<0){
    _udx_seterror(UDX_READ);
    return(0);
  }
  return(res);
}

static int can_write_sock(int fd)
{
  int res=0;
  struct timeval to;
  struct timeval * to_ptr;
  fd_set writefds;
  FD_ZERO(&writefds);
  FD_SET((unsigned int)fd,&writefds);
  if (get_write_timeout()>-1) {  
    to.tv_sec=get_read_timeout();
    to.tv_usec=0;
    to_ptr=&to;
  }
  else {
    to_ptr=NULL;
  }
  if ((res=select(fd+1,0,&writefds,0,to_ptr)) == -1)
    {
      _udx_seterror(UDX_SELECT);
      return -1;
    }
  if (FD_ISSET(fd,&writefds)) {
    return(1);
  }  
  _udx_seterror(UDX_TIMEOUT_WRITE);
  return(0);
}

int real_write_sock(char * out1,const int ofset,const int to_send,const int fd) {
  int res;
  while ((_udx_noerror()) && (can_write_sock(fd)< 1)) {};
  if(_udx_error) return(0);
  res=send(fd,(char *) (out1 + ofset),to_send,0);
  if(res<0) {
    _udx_seterror(UDX_WRITE);
    return(0);
  }
  else return(res);
}


#endif /* NO_UDX_SOCK */
