#ifndef NO_UDX_SOCK
#include <stdio.h>
#include <stdlib.h>
#ifdef WIN32
#ifdef CYGWIN
#define Win32_Winsock
#include "windows.h"
#else
#ifndef WINSOCK2
#include <winsock.h>
#else 
#include <winsock2.h>
#endif
#endif
#else
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif
#include <time.h>
#include <fcntl.h>
#include "udx_def.h"

#ifndef __udx_sock_h
#define __udx_sock_h

/* pour un premier message du client vers le serveur */
#define NO_SYN 0
#define SYN 1<<3
#define SYN_MASK 1<<3

extern int open_sock_server(const UDX_STRING,const UDX_UI16, 
			    const UDX_UI16 ,const UDX_UI16, 
			    struct sockaddr_in *,struct timeval *);
extern int open_sock_client(const UDX_STRING,  UDX_UI16, const UDX_UI16 ,
			    const UDX_UI16, struct sockaddr_in *);
extern int accept_sock(int,struct sockaddr_in *,struct timeval * );
extern int real_read_sock(char *,const int,const int,const int);
extern int real_write_sock(char *,const int,const int,const int);
#endif
#endif
