#include "udx_stream.h"

int (*detect_ieee)(void)=detect_ieee_patent;
int (*detect_long)(void)=detect_long_patent;

#define UDX_LOCAL_MAX_SIZE sizeof(long)

static int activate__udx_io_sock(_udx_io_rep *dst,struct sockaddr_in * corr,struct timeval to_1)
{
  int tmp_fd;
  if (dst->ready == 0) {
   tmp_fd=accept_sock(dst->fd,corr,&(to_1));
    if (tmp_fd>0) {
      dst->ready=1;
      dst->fd=tmp_fd;
    }
    else {
      _udx_seterror(1);
      dst->ready=0;
    }
  }
  return(dst->ready);
}


int _udx_init_file_negoc(_udx_stream_rep *s, int fi)
{
#ifdef WIN32
  _fmode=_O_BINARY;
#endif
  init__udx_io(&((s)->i),(fi),1,read_init_infos_int_negoc,
	      send_init_infos_int_negoc,real_read_file,
	      real_write_file,UDX_LOCAL_MAX_SIZE);
  init__udx_ieee(&((s)->i),&((s)->f),read_init_infos_ieee_negoc,
	       send_init_infos_ieee_negoc);
  return(1);
}


int _udx_init_tcp_client_negoc_0(_udx_stream_rep *s,char *mach,char *pat,char *prog,char *param, struct timeval to_1)
{
  int fd=0;
  UDX_UI16 pn=0;
  struct sockaddr_in corresp;
  fd=open_sock_server(mach,0,SOCK_STREAM,0,&(corresp),&(to_1));
  pn=ntohs(corresp.sin_port);
  init__udx_io(&((s)->i),fd,0,read_init_infos_int_negoc,
	      send_init_infos_int_negoc,real_read_sock,
	      real_write_sock,UDX_LOCAL_MAX_SIZE);
  run_server(pat,prog,mach,param,pn);
  activate__udx_io_sock(&(s->i),&(corresp),to_1);
  init__udx_ieee(&((s)->i),&((s)->f),read_init_infos_ieee_negoc,
	       send_init_infos_ieee_negoc);
  return(1);
}

int _udx_init_tcp_client_negoc_1(_udx_stream_rep *s,char *mach,const int pn, struct timeval to_1)
{
  int fd=0;
  struct sockaddr_in corresp;
  fd=open_sock_server(mach,(UDX_UI16) pn,(UDX_UI16)SOCK_STREAM,(UDX_UI16) 0,&(corresp),&(to_1));
  printf("Connected to %s:%d\n", mach, ntohs(corresp.sin_port));
  init__udx_io(&((s)->i),fd,0,read_init_infos_int_negoc,
	      send_init_infos_int_negoc,real_read_sock,
	      real_write_sock,UDX_LOCAL_MAX_SIZE);
  activate__udx_io_sock(&(s->i),&(corresp),to_1);
  init__udx_ieee(&((s)->i),&((s)->f),read_init_infos_ieee_negoc,
	       send_init_infos_ieee_negoc);
  return(1);
}


int _udx_init_tcp_server_negoc(_udx_stream_rep *s,char * mach,const int pn,struct timeval to_1)
{
  struct sockaddr_in corresp;
  int fd=0;
  fd=open_sock_client(mach,(UDX_UI16)pn,(UDX_UI16)SOCK_STREAM,(UDX_UI16)0,&(corresp));
  init__udx_io(&((s)->i),fd,1,read_init_infos_int_negoc,
	      send_init_infos_int_negoc,real_read_sock,
	      real_write_sock,UDX_LOCAL_MAX_SIZE);
  init__udx_ieee(&((s)->i),&((s)->f),read_init_infos_ieee_negoc,
	       send_init_infos_ieee_negoc);  
  return(1);
}


int _udx_init_file(_udx_stream_rep *c, int fd)
{
    return(_udx_init_file_negoc(c,fd));
}

int _udx_init_tcp_master_run(_udx_stream_rep *c,char *mach,char *pat,char *prog,char *param, struct timeval to_1)
{
    return(_udx_init_tcp_client_negoc_0(c,mach,pat,prog,param,to_1));
}

int _udx_init_tcp_master(_udx_stream_rep *c,char *mach,const int pn, struct timeval to_1)
{
    return(_udx_init_tcp_client_negoc_1(c,mach,pn,to_1));
}

int _udx_init_tcp_slave(_udx_stream_rep *c,char *mach,const int pn, struct timeval to_1)
{
    return(_udx_init_tcp_server_negoc(c,mach,pn,to_1));
}
