#ifndef __udx_stream_h
#define __udx_stream_h

#include "udx_udx.h"
#include "udx_ieee.h"
#include "udx_file.h"
#include "udx_sock.h"
#include "udx_tools.h"
#include "udx_mp.h"


typedef struct {
  _udx_io_rep   i; /* basic channel           - active */
  _udx_ieee_rep f; /* ieee channel            - active */
  _udx_mp_rep m;   /* multi-precision channel - not active */
}_udx_stream_rep;

typedef _udx_stream_rep _udx_stream[1];


extern int _udx_init_file(_udx_stream_rep *, int);
extern int _udx_init_tcp_master_run(_udx_stream_rep *,char *,char *,char *,
				   char *, struct timeval);
extern int _udx_init_tcp_master(_udx_stream_rep *,char *,const int, 
			       struct timeval);
extern int _udx_init_tcp_slave(_udx_stream_rep *,char *,const int, 
			      struct timeval);

/* *************************************************************************
   INITIALIZATIONS FOR FILES
   ************************************************************************* */



extern int _udx_init_file_negoc(_udx_stream_rep *, int);

/* *************************************************************************
   INITIALIZATIONS FOR TCP
   ************************************************************************* */

/* ------------------------------------------------- */
/* open an UDX stream for tcp exchange in negoc mode */
/* ------------------------------------------------- */

/* 
   _udx_init_tcp_client_negoc_0 
       the current process is considered as the master.
       the function creates and open a socket, 
       run a slave program that will bind on the socket 
       for exchanging data. The 2 first arguments passed to 
       the slave (command line) are respectively the name of 
       the machine where the master runs and the socket number
       created by the master.
       1rst argument : udx stream to be initialized
       2nd argument  : target machine where the slave will run
       3rd argument  : full path (directory) where to find
                       the executable (slave) must end with 
                       a "/" (unix) or a "\" (windows) 
       4th argument  : name of the executable file (slave) 
       5th argument  : extra parameters to be given to the slave
                       (in addition to the 2 firsts which are 
                        the machine's name where the master runs 
			and the socket number
			created by the master)
       6th argument  : timeout value (maximum wait for an answer  
                       after the slave start)
*/

extern int _udx_init_tcp_client_negoc_0(_udx_stream_rep *,char *,char *,char *,
				       char *, struct timeval);
/* 
   _udx_init_tcp_client_negoc_1
       the current process is considered as the master.
       the function creates and open a socket of a fixed number,
       then wait for the connection of a slave.
       1rst argument : udx stream to be initialized
       2nd argument  : target machine where the slave will run
       3rd argument  : socket number
       4th argument  : timeout value (maximum wait for an answer  
                       after the slave start)
*/

extern int _udx_init_tcp_client_negoc_1(_udx_stream_rep *,char *,const int, 
				       struct timeval);

/* 
   _udx_init_tcp_server_negoc
       the current process is considered as the slave.
       the function binds an existing socket of a fixed number.
       1rst argument : udx stream to be initialized
       2nd argument  : target machine where the master run
       3rd argument  : socket number
       4th argument  : timeout value (maximum wait for an answer  
                       after the slave start)
*/

extern int _udx_init_tcp_server_negoc(_udx_stream_rep *,char *,const int,
				     struct timeval);


#define _udx_sflush(a) _udx_flush((&(a->i)))

/* *************************************************************************
   Multiprecision channel
   ************************************************************************* */

/* extern int _udx_set_mp(_udx_stream_rep *,const UDX_UI32, const UDX_UI32,const UDX_UI32); s_limb,e_limb,ordre limb format externe,limb format interne, 1 pour GMP (low first) */
#define _udx_set_mpi(c,el,o)\
  new__udx_mp(&(c->i),&(c->m),el,8,o)

#define read_mpi(c,limb_ptr,nb_limbs)\
read_mp(&(c->i),&(c->m),limb_ptr,nb_limbs)

#define send_mpi(c,limb_ptr,nb_limbs)\
send_mp(&(c->i),&(c->m),limb_ptr,nb_limbs)


/* *************************************************************************
   MACROS for READING/WRITING
   ************************************************************************* */

/* 
   read_f_ieee
   read a unique FLOAT IEEE 754 (32 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where to store the result
*/

#define read_f_ieee(s,r) \
read_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(float),sizeof(float))

/* 
   read_f_ieee_n
   read an array of FLOAT IEEE 754 (32 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_f_ieee_n(s,r,nb) \
read_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(float),(nb)*sizeof(float))

/* 
   send_f_ieee
   send a unique FLOAT IEEE 754 (32 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_f_ieee(s,r) \
send_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(float),sizeof(float))

/* 
   send_f_ieee_n
   send an array of FLOAT IEEE 754 (32 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_f_ieee_n(s,r,nb) \
send_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(float),(nb)*sizeof(float))

/* 
   read_d_ieee
   read a unique DOUBLE IEEE 754 (64 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where to store the result
*/

#define read_d_ieee(s,r) \
read_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(double),sizeof(double))

/* 
   read_d_ieee_n
   read an array of DOUBLE IEEE 754 (64 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_d_ieee_n(s,r,nb) \
read_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(double),(nb)*sizeof(double))

/* 
   send_d_ieee
   send a unique DOUBLE IEEE 754 (64 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_d_ieee(s,r) \
send_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(double),sizeof(double))

/* 
   send_d_ieee_n
   send an array of DOUBLE IEEE 754 (64 bit) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_d_ieee_n(s,r,nb) \
send_n_ieee(&((s)->i),&((s)->f),(UDX_IT *) (r),sizeof(double),(nb)*sizeof(double))

/* 
   read_i8
   read a 8 bit integer (signed or not)
   1rst argument : the UDX STREAM
   2nd  argument : the adress where to store the result
*/

#define read_i8(s,r) \
read_n_int(&((s)->i),(UDX_IT *) (r),1,1)

/* 
   read_i8_n
   read an array of 8 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_i8_n(s,r,nb) \
read_n_int(&((s)->i),(UDX_IT *) (r),1,(nb))

/* 
   send_i8
   send a unique 8 bit integer (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_i8(s,r) \
send_n_int(&((s)->i),(UDX_IT *) (r),1,1)

/* 
   send_i8_n
   send an array of 8 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_i8_n(s,r,nb) \
send_n_int(&((s)->i),(UDX_IT *) (r),1,(nb))

/* 
   read_i16
   read a 16 bit integer (signed or not)
   1rst argument : the UDX STREAM
   2nd  argument : the adress where to store the result
*/

#define read_i16(s,r) \
read_n_int(&((s)->i),(UDX_IT *) (r),2,2)

/* 
   read_i16_n
   read an array of 16 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_i16_n(s,r,nb) \
read_n_int(&((s)->i),(UDX_IT *) (r),2,2*(nb))

/* 
   send_i16
   send a unique 16 bit integer (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_i16(s,r) \
send_n_int(&((s)->i),(UDX_IT *) (r),2,2)

/* 
   send_i16_n
   send an array of 16 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_i16_n(s,r,nb) \
send_n_int(&((s)->i),(UDX_IT *) (r),2,2*(nb))

/* 
   read_i32
   read a 32 bit integer (signed or not)
   1rst argument : the UDX STREAM
   2nd  argument : the adress where to store the result
*/

#define read_i32(s,r) \
read_n_int(&((s)->i),(UDX_IT *) (r),4,4)

/* 
   read_i32_n
   read an array of 32 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_i32_n(s,r,nb) \
read_n_int(&((s)->i),(UDX_IT *) (r),4,4*(nb))

/* 
   send_i32
   send a unique 32 bit integer (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_i32(s,r) \
send_n_int(&((s)->i),(UDX_IT *) (r),4,4)

/* 
   send_i32_n
   send an array of 32 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_i32_n(s,r,nb) \
send_n_int(&((s)->i),(UDX_IT *) (r),4,4*(nb))



#define read_i64(s,r) \
read_n_int(&((s)->i),(UDX_IT *) (r),4,4)

/* 
   read_i64_n
   read an array of 64 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the first element) 
                   where to store the result
   3rd  argument : the number of elements to be read
*/

#define read_i64_n(s,r,nb) \
read_n_int(&((s)->i),(UDX_IT *) (r),4,4*(nb))

/* 
   send_i64
   send a unique 64 bit integer (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress where the data are stored 
*/

#define send_i64(s,r) \
send_n_int(&((s)->i),(UDX_IT *) (r),4,4)

/* 
   send_i64_n
   send an array of 64 bit integers (signed or not) on a UDX STREAM
   1rst argument : the UDX STREAM
   2nd  argument : the adress (of the array) where the data are stored 
   3rd  argument : the number of elements to be sent
*/

#define send_i64_n(s,r,nb) \
send_n_int(&((s)->i),(UDX_IT *) (r),4,4*(nb))

#endif /* __udx_stream_h */
