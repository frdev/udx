#include "udx_tools.h"

void get_timings(double *tu,double * ts)
{
#ifdef WIN32
	clock_t finish=clock();
	(*tu) =(double)(finish) / CLOCKS_PER_SEC;
	(*ts)=0;
#else
  struct rusage r_fin;
  getrusage( RUSAGE_SELF,&r_fin);
  (*tu)=(r_fin.ru_utime.tv_sec+ r_fin.ru_utime.tv_usec/1000000.0);
  (*ts)=(r_fin.ru_stime.tv_sec+ r_fin.ru_stime.tv_usec/1000000.0);
#endif
}

int run_server(char * path,char * server, char *remote,char* options,int port)
{
  char output[500];
#ifdef WIN32
  int nbargs=0;
  char * args[100];
  char aport[10];
#endif
  char this_mach[255];
  /*  char this_dom[255];*/
  char remote_mach[255];
  char * rmt_cmd;
  struct hostent t_ent[1],t_ent2[1],*ent,*ent2;
  ent=t_ent;ent2=t_ent2;
  rmt_cmd=getenv("RSREMOTECMD");
  if (gethostname(this_mach,255)<0) strcpy(this_mach,"localhost");
  ent=gethostbyname(this_mach);
  if (ent == 0)     strcpy(this_mach,(gethostbyname("localhost"))->h_name);
  else     strcpy(this_mach,(gethostbyname(this_mach))->h_name);    
  if(strcmp(remote,"")){
    if(strcmp(remote,"localhost")){
      ent2=gethostbyname(remote);
      if (ent2 == 0) strcpy(remote_mach,(gethostbyname("localhost"))->h_name);
      else  strcpy(remote_mach,(gethostbyname(remote))->h_name);
    }
    else {
      strcpy(remote_mach,this_mach);
    }
  }
  else {
    strcpy(remote_mach,this_mach);
  }
  fprintf(stderr,"%s \n %s\n",this_mach,remote_mach);
  if(strcmp(this_mach,remote_mach)) {
    if(rmt_cmd==NULL) {
      rmt_cmd="rsh";
    }
#ifndef WIN32
    sprintf(output,"%s %s %s%s %s %s %d &",rmt_cmd,remote_mach,path,server,options,this_mach,port);
#else
    args[0]="rsh";
    args[1]=remote_mach;
    sprintf(output,"%s%s",path,server);
    args[2]=output;
    if(strcmp(options,"")) {
      args[3]=options;
      args[4]=this_mach;
      sprintf(aport,"%d",port);
      args[5]=&(aport[0]);
      args[6]=NULL;
    }
    else {
      args[3]=this_mach;
      sprintf(aport,"%d",port);
      args[4]=&(aport[0]);
      args[5]=NULL;
    }
#endif    
  }
  else {
    if(rmt_cmd==NULL) {
      rmt_cmd="exec";
    }
#ifndef WIN32
    sprintf(output,"%s %s%s %s %s %d &",rmt_cmd,path,server,options,this_mach,port);
#else
    sprintf(output,"%s%s",path,server);
    args[0]=output;
    if(strcmp(options,"")) {
      args[1]=options;
      args[2]=this_mach;
      sprintf(aport,"%d",port);
      args[3]=&(aport[0]);
      args[4]=NULL;
    }
    else {
      args[1]=this_mach;
      sprintf(aport,"%d",port);
      args[2]=&(aport[0]);
      args[3]=NULL;
    }
#endif    
  }
#ifndef WIN32
  fprintf(stderr,"%s",output);
  return((system(output)!=-1));
#else 
  return(_spawnvp(_P_NOWAIT,args[0],args));
#endif
}

int run_server_test(char * path,char * server, char *remote,char* options,int port)
{
  fprintf(stderr,"\n toto\n");
  return(1);
}


int run_server_pipe(const char * cmd,const int fd_r,const int fd_w)
{
  char output[1000];
  sprintf(output,"%s %d %d &",cmd,fd_r,fd_w);
  fprintf(stderr,"\n Master run %s \n",output);
  return((system(output)!=-1));
}

