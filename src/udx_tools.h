#ifndef __udx_tools_h
#define __udx_tools_h
#include "udx_sock.h"

#include <stdlib.h>
#include <stdio.h>

#ifndef WIN32
#include <sys/time.h>
#include <sys/resource.h>                                                
#include <unistd.h> 
#else
#undef getpid
#include <process.h>
#endif

extern void get_timings(double *,double *);
extern int run_server(char *,char *, char *,char*,int);
extern int run_server_pipe(const char *,const int,const int);

#endif
