#include "udx_udx.h"

/****************************************************************
Send Init 
 ****************************************************************/

int send_init_infos_int_negoc(_udx_io_rep *dst)
     /* envoie la taille max des entiers compris par le protocole 
        suivi des permutations interne_to_xdr */
{
  UDX_UI32 ok=0;
  unsigned int xdr_max_prot_size=0,i;
  full_init_udx();
  if(_udx_error) return(0);
  initialise_tableau(dst->tab_interne_to_externe);
  UDX_HTONL(xdr_max_prot_size,dst->max_local_size);
  send_n_p(dst,(UDX_IT *) &xdr_max_prot_size,4,4,UDX_PTR_NULL);
  for (i=1;i<=dst->max_local_size;i*=2) {
    if(_udx_local_infos->tab_interne_to_xdr[i] != UDX_PTR_NULL) ok=1;
    else ok=0;
    send_n_p (dst,(UDX_IT *)(&ok),4,4,UDX_PTR_NULL);
    if (ok) 
	send_n_p (dst,(UDX_IT *)(_udx_local_infos->tab_interne_to_xdr[i]),1,i,UDX_PTR_NULL);
      /* on ecrit toujours dans son propre format */
    dst->tab_interne_to_externe[i]=UDX_PTR_NULL;
  }
  _udx_flush(dst);
  return(1);
}

/****************************************************************
 READ Init 
 ****************************************************************/

int read_init_infos_int_negoc(_udx_io_rep *dst)
     /* lit la taille max du protocole puis 
        les permutations interne_to_xdr du process target */
{
  UDX_UI32 ok=0;
  UDX_UI32 max_prot_size=0;
  UDX_UI32 xdr_max_prot_size=0,i;
  full_init_udx();
  read_n_p(dst,(UDX_IT *) &xdr_max_prot_size,4,4,UDX_PTR_NULL);
  UDX_NTOHL(max_prot_size,xdr_max_prot_size);
  initialise_tableau(dst->tab_externe_to_interne);
  for (i=1;i<=max_prot_size;i*=2) {
    read_n_p (dst,(UDX_IT *)(&ok),4,4,UDX_PTR_NULL);
    dst->tab_externe_to_interne[i]=(UDX_IT *)UDX_ALLOC_ATOMIC(i);
    /* lit le interne_to_xdr du process target */
    if (ok!=0) {
      /* format target <> XDR */
      read_n_p(dst,(UDX_IT *) (dst->tab_externe_to_interne[i]),1,i,UDX_PTR_NULL);
      if (_udx_local_infos->tab_interne_to_xdr[i]) {
	/* format local <>XDR */
	if((UDX_MEMCMP((char *)(_udx_local_infos->tab_interne_to_xdr[i]),(char *)(dst->tab_externe_to_interne[i]),i))){
	  /* si interne_to_xdr target <> interne_to_xdr interne_to_xdr local */
	  dst->tab_externe_to_interne[i]=comp_permut(dst->tab_externe_to_interne[i],_udx_local_infos->tab_xdr_to_interne[i],i);
	}
	else {
	  /* si interne_to_xdr target == interne_to_xdr interne_to_xdr local */
	  dst->tab_externe_to_interne[i]=UDX_PTR_NULL;
	}
      }
      else {
	/* format local = xdr */
	/* ne rien faire c'est deja fait */
      }
    }
    else {
      /* format target = XDR */
      if (_udx_local_infos->tab_xdr_to_interne[i]) {
	UDX_MEMCPY(dst->tab_externe_to_interne[i],_udx_local_infos->tab_xdr_to_interne[i],i);
      }
      else dst->tab_externe_to_interne[i]=UDX_PTR_NULL;
    }
  }
  dst->max_target_size=max_prot_size;
  return(1);
}

