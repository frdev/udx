#ifndef __udx__udx_h
#define __udx__udx_h

/* Protocole Negociated mode */

#include "udx_io.h"
#include "udx_patent.h"

extern int  send_init_infos_int_negoc(_udx_io_rep *);
extern int  read_init_infos_int_negoc(_udx_io_rep *);

#endif
