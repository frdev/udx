/****A************************************************************
  Using UDX
****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "udx_stream.h"
#include "udx_tools.h"
#include "udx_gmp.h"

#if 0
/* mpz 64 little endian */
int udx_send_mpz(udx_stream_rep *c,mpz_srcptr tmp1)
{
  int i=0;
  int tmp=tmp1->_mp_size;
  send_i32(c,(UDX_IT *)(&tmp));
  if(tmp<0) tmp=-tmp;
  send_n_int(&((c)->i),(UDX_IT *) (tmp1->_mp_d),8,8*(tmp));
  return(1);
}

/* mpz 64 little endian */
int udx_read_mpz(udx_stream_rep *c,mpz_ptr tmp1)
{
  int tmp=0;
  int i=0;
  mpz_clear(tmp1);
  read_i32(c,(UDX_IT *)(&tmp));
  if (tmp<0) tmp1->_mp_alloc=-(tmp);
  else tmp1->_mp_alloc=tmp;
  mpz_init2(tmp1,(tmp1->_mp_alloc)*(sizeof(mp_limb_t))*8);
  read_n_int(&((c)->i),(UDX_IT *) (tmp1->_mp_d),8,8*(tmp1->_mp_alloc));
  if(tmp1->_mp_alloc>0){
    if(tmp<0) tmp1->_mp_size=-(tmp1->_mp_alloc);
    else tmp1->_mp_size=tmp1->_mp_alloc;
  }
  else {
    tmp1->_mp_size=0;
  }
  return(1);
}
#endif

#ifdef CLIENT_UDX

int main(int argc, char ** argv)
{
  mpz_t tmp;
  /* initialise le udx_channel */
  _udx_stream channel;
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  fprintf(stderr,"\nClient init %d \n",atoi(argv[2]));
  _udx_init_tcp_server_negoc(channel,"localhost",
				        atoi(argv[2]),
                                        to_1);
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  int32_t vv=0;
  read_i32(channel,(&(vv)));
  fprintf(stderr,"\nClient got %d \n",vv); 
  vv*=2;
  send_i32(channel,(&(vv)));
  _udx_sflush(channel);
  mpz_init(tmp);
  _udx_read_mpz(channel,tmp);
  fprintf(stderr,"\nClient got : ");
  mpz_out_str(stderr,10,tmp);
  fprintf(stderr,"\n");
  mpz_mul_ui(tmp,tmp,2);
  _udx_send_mpz(channel,tmp);
  _udx_sflush(channel);
  fprintf(stderr,"\nClient flush \n"); 
}

#else

int main(int argc, char** argv)
{
  /* initialise le udx_channel */
  _udx_stream channel;  
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  mpz_t tmp,tmp2;

  _udx_init_tcp_client_negoc_0(channel,"localhost",
			              "../build/",
			              "client_udx","",to_1);
  /* initialise le udx_mp */
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  /* initialise un entier 32 bit */
  int32_t uu=123,vv=0;
  /* envoie l'entier 32 bit */
  send_i32(channel,(&(uu)));
  _udx_sflush(channel);
  /* receptionne l'entier 32 bit */
  read_i32(channel,(&(vv)));
  /* vérifie l'entier 32 bit */
  fprintf(stderr,"\nMaster got %d \n",vv);
  assert (2*uu== vv);
  /* cree un gmp */
  mpz_init_set_str(tmp,"123456789101121314151617181920212222310987678968675567465434523",10);
  /* envoie le gmp */
  _udx_send_mpz(channel,tmp);
  _udx_sflush(channel);
  /* receptionne le resultat */
  mpz_init(tmp2);
  _udx_read_mpz(channel,tmp2);

  fprintf(stderr,"\nMaster got : ");
  mpz_out_str(stderr,10,tmp2);
  fprintf(stderr,"\n");

  mpz_mul_ui(tmp,tmp,2);
  mpz_sub(tmp,tmp,tmp2);
  /*  mpz_out_str(stderr,10,tmp2);*/
  /* verifie le resultat */
  assert(mpz_cmp_ui(tmp,0)==0);
  fprintf(stderr,"\nTEST OK\n");
  return(1);
}

#endif

