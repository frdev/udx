/****A************************************************************
  Using UDX
****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "udx_stream.h"
#include "udx_tools.h"
#include "mpfi.h"
#include "mpfi_io.h"
#include "udx_mpfi.h"
#include "udx.h"
#if 0

// Server code

#endif

#ifdef CLIENT_UDX

int main(int argc, char ** argv)
{
  mpfi_t tmp;
  // Initialize UDX connection channel
  _udx_stream channel;
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  fprintf(stderr,"\nClient init %d \n",atoi(argv[2]));
  _udx_init_tcp_server_negoc(channel,"localhost",
				        atoi(argv[2]),to_1);
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  int32_t vv=0;
  read_i32(channel,(&(vv)));
  fprintf(stderr,"\nClient got %d \n",vv); 
  vv*=2;
  send_i32(channel,(&(vv)));
  _udx_sflush(channel);
  mpfi_init(tmp);
  udx_read_mpfi(channel,tmp);
  fprintf(stderr,"\nClient got : ");
  mpfi_out_str(stderr,10,10, tmp);
  fprintf(stderr,"\n");
  mpfi_mul_ui(tmp,tmp,2);
  udx_write_mpfi(channel,tmp);
  _udx_sflush(channel);
  fprintf(stderr,"\nClient flush \n"); 
}

#else

int main(int argc, char** argv)
{
  // Initialize UDX connection channel
  // channel is essentially a stream structure with three channels: io, ieee, and mp
  _udx_stream channel;
  // Timeout for UDX connection
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  mpfi_t tmp,tmp2;

    // inside the function, the channels udx_io and udx_ieee are initialized
  _udx_init_tcp_client_negoc_0(channel,"localhost",
			              "../build/",
			              "client_mpfi","",to_1);
    // since we work with multiprecision numbers, we need to set the mp channel
    // udx_set_mpi is a macro that simplifies the configuration of a UDX stream for handling multiple precision integers. 
    // It sets up the necessary parameters and ensures that the correct read and send functions are used based on the limb size. 
    // Here, it sets read_mp and send_mp as read_mp_int and send_mp_int respectively.
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  // initialise un entier 32 bit
  int32_t uu=246,vv=0;
  // envoie l'entier 32 bit
  send_i32(channel,(&(uu)));
  _udx_sflush(channel);
  // receptionne l'entier 32 bit
  read_i32(channel,(&(vv)));
  /* vérifie l'entier 32 bit */
  fprintf(stderr,"\nMaster got %d \n",vv);
  assert (2*uu== vv);
  /* cree un mpfi */
  mpfi_init(tmp);
  mpfi_set_str(tmp, "-123456789101121314151617181920212222310987678968675567465434523", 10);
  fprintf(stderr,"\nMaster will send : ");
  mpfi_out_str(stderr,10,10, tmp);
  fprintf(stderr,"\n");
  /* envoie le mpfi */
  udx_write_mpfi(channel,tmp);
  _udx_sflush(channel);
  /* receptionne le resultat */
  mpfi_init(tmp2);
  udx_read_mpfi(channel,tmp2);

  fprintf(stderr,"\nMaster got the mpfi: ");
  mpfi_out_str(stderr,10,10, tmp2);
  fprintf(stderr,"\n");

  mpfi_mul_ui(tmp,tmp,2);
  mpfi_sub(tmp,tmp,tmp2);
  /*  mpz_out_str(stderr,10,tmp2);*/
  /* verifie le resultat */
  assert(mpfi_cmp_ui(tmp,0)==0);
  fprintf(stderr,"\nTEST OK\n");
  return(0);
}

#endif

