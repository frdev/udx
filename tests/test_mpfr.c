/****A************************************************************
  Using UDX
****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "udx_stream.h"
#include "udx_tools.h"
#include "mpfr.h"
#include "udx_mpfr.h"

#if 0

// Server code

#endif

#ifdef CLIENT_UDX

int main(int argc, char ** argv)
{
  mpfr_t tmp;
  // Initialize UDX connection channel
  _udx_stream channel;
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  fprintf(stderr,"\nClient init %d \n",atoi(argv[2]));
  _udx_init_tcp_server_negoc(channel,"localhost",
				        atoi(argv[2]),to_1);
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  int32_t vv=0;
  read_i32(channel,(&(vv)));
  fprintf(stderr,"\nClient got %d \n",vv); 
  vv*=2;
  send_i32(channel,(&(vv)));
  _udx_sflush(channel);
  mpfr_init(tmp);
  udx_read_mpfr(channel,tmp);
  fprintf(stderr,"\nClient got : ");
  mpfr_out_str(stderr,10,10, tmp, GMP_RNDN);
  fprintf(stderr,"\n");
  mpfr_mul_ui(tmp,tmp,2, MPFR_RNDN);
  udx_write_mpfr(channel,tmp);
  _udx_sflush(channel);
  fprintf(stderr,"\nClient flush \n"); 
}

#else

int main(int argc, char** argv)
{
  // Initialize UDX connection channel
  // channel is essentially a stream structure with three channels: io, ieee, and mp
  _udx_stream channel;
  // Timeout for UDX connection
  struct timeval to_1;
  to_1.tv_sec=10;
  to_1.tv_usec=0;
  mpfr_t tmp,tmp2;

  // Initialize UDX connection: 
  // This is a master-slave setup where the master launches and connects to a slave program.
    // the current process is considered as the master.
    //    the function creates and open a socket, 
    //    run a slave program that will bind on the socket 
    //    for exchanging data. The 2 first arguments passed to 
    //    the slave (command line) are respectively the name of 
    //    the machine where the master runs and the socket number
    //    created by the master.
    //    1rst argument : udx stream to be initialized
    //    2nd argument  : target machine where the slave will run
    //    3rd argument  : full path (directory) where to find
    //                    the executable (slave) must end with 
    //                    a "/" (unix) or a "\" (windows) 
    //    4th argument  : name of the executable file (slave) 
    //    5th argument  : extra parameters to be given to the slave
    //                    (in addition to the 2 firsts which are 
    //                     the machine's name where the master runs 
	// 		and the socket number
	// 		created by the master)
    //    6th argument  : timeout value (maximum wait for an answer  
    //                    after the slave start)

    // inside the function, the channels udx_io and udx_ieee are initialized
  _udx_init_tcp_client_negoc_0(channel,"localhost",
			              "../build/",
			              "client_mpfr","",to_1);
    // since we work with multiprecision numbers, we need to set the mp channel
    // udx_set_mpi is a macro that simplifies the configuration of a UDX stream for handling multiple precision integers. 
    // It sets up the necessary parameters and ensures that the correct read and send functions are used based on the limb size. 
    // Here, it sets read_mp and send_mp as read_mp_int and send_mp_int respectively.
  _udx_set_mpi(channel,sizeof(mp_limb_t),1);
  // initialise un entier 32 bit
  int32_t uu=246,vv=0;
  // envoie l'entier 32 bit
  send_i32(channel,(&(uu)));
  _udx_sflush(channel);
  // receptionne l'entier 32 bit
  read_i32(channel,(&(vv)));
  /* vérifie l'entier 32 bit */
  fprintf(stderr,"\nMaster got %d \n",vv);
  assert (2*uu== vv);
  /* cree un mpfr */
  mpfr_init_set_str(tmp, "-123456789101121314151617181920212222310987678968675567465434523", 10, MPFR_RNDN);
  fprintf(stderr,"\nMaster will send : ");
  mpfr_out_str(stderr,10,10, tmp, GMP_RNDN);
  fprintf(stderr,"\n");
  /* envoie le mpfr */
  udx_write_mpfr(channel,tmp);
  _udx_sflush(channel);
  /* receptionne le resultat */
  mpfr_init(tmp2);
  udx_read_mpfr(channel,tmp2);

  fprintf(stderr,"\nMaster got : ");
  mpfr_out_str(stderr,10,10, tmp2, GMP_RNDN);
  fprintf(stderr,"\n");

  mpfr_mul_ui(tmp,tmp,2, MPFR_RNDN);
  mpfr_sub(tmp,tmp,tmp2, MPFR_RNDN);
  /*  mpz_out_str(stderr,10,tmp2);*/
  /* verifie le resultat */
  assert(mpfr_cmp_ui(tmp,0)==0);
  fprintf(stderr,"\nTEST OK\n");
  return(0);
}

#endif

